<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 01.12.2017
 * Time: 11:43
 */

namespace app\assets;
use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "assets/global/plugins/font-awesome/css/font-awesome.min.css",
        "assets/global/plugins/simple-line-icons/simple-line-icons.min.css",
        "assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
        "assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
        "assets/global/css/components.min.css",
        "assets/global/css/plugins.min.css",
        "assets/layouts/layout/css/layout.min.css",
        "assets/layouts/layout/css/themes/darkblue.min.css",
        "assets/layouts/layout/css/custom.min.css",
    ];
    public $js = [
        "assets/global/plugins/bootstrap/js/bootstrap.min.js",
        "assets/global/plugins/js.cookie.min.js",
       "assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
        "assets/global/plugins/jquery.blockui.min.js",
        "assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
        "assets/global/scripts/app.min.js",
        "assets/layouts/layout/scripts/layout.min.js",
        "assets/layouts/layout/scripts/demo.min.js",
        "assets/layouts/global/scripts/quick-sidebar.min.js",
        "assets/layouts/global/scripts/quick-nav.min.js",
        "assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js",
        "js/admin-main.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}