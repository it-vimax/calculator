<?php

namespace app\models;
use mdm\admin\models\User as UserModel;

class User extends UserModel
{
    public static function getUsername($id){
        $model = static::findOne(['id' => $id]);
        return $model->username;
    }
}
