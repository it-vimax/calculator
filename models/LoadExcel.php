<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 30.11.2017
 * Time: 9:16
 */

namespace app\models;


use yii\base\Model;

class LoadExcel extends Model
{
    public $excel;
    
    public function upload()
    {
        $this->excel->saveAs('@app/excel/' . $this->excel->baseName . '.' . $this->excel->extension);
        return true;
    }
}