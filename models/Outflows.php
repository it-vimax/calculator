<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "outflows".
 *
 * @property integer $id
 * @property integer $depth
 * @property string $color
 * @property string $price
 */
class Outflows extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outflows';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['depth'], 'integer'],
            [['price'], 'double'],
            [['color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'depth' => 'Depth',
            'color' => 'Color',
            'price' => 'Price',
        ];
    }
}
