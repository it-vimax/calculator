<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "additional_works".
 *
 * @property integer $id
 * @property string $name
 * @property integer $value
 */
class AdditionalWorks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'additional_works';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'double'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }
}
