<?php

use yii\helpers\Url;

?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">

        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <li class="nav-item <?= ('/'.Yii::$app->request->pathInfo == Url::to(['default/index'])) ? "start active open" : "" ?>">
                <a href="<?= Url::to(['default/index', ]) ?>" class="nav-link nav-toggle">
                    <i class="icon-feed"></i>
                    <span class="title">Обратная связь</span>
                    <?php if('/'.Yii::$app->request->pathInfo == Url::to(['default/index'])): ?>
                        <span class="selected"></span>
                    <?php endif; ?>
                </a>
            </li>
            <li class="nav-item <?= ('/'.Yii::$app->request->pathInfo == Url::to(['window-items/index'])) ? "start active open" : "" ?>">
                <a href="<?= Url::to(['window-items/index', ]) ?>" class="nav-link nav-toggle">
                    <i class="icon-puzzle"></i>
                    <span class="title">Компоненты окна</span>
                    <?php if('/'.Yii::$app->request->pathInfo == Url::to(['window-items/index'])): ?>
                        <span class="selected"></span>
                    <?php endif; ?>
                </a>
            </li>
            <li class="nav-item <?= ('/'.Yii::$app->request->pathInfo == Url::to(['additional-works/index'])) ? "start active open" : "" ?>">
                <a href="<?= Url::to(['additional-works/index', ]) ?>" class="nav-link nav-toggle">
                    <i class="icon-briefcase"></i>
                    <span class="title">Дополнительные работы</span>
                    <?php if('/'.Yii::$app->request->pathInfo == Url::to(['additional-works/index'])): ?>
                        <span class="selected"></span>
                    <?php endif; ?>
                </a>
            </li>
            <li class="nav-item <?= ('/'.Yii::$app->request->pathInfo == Url::to(['/admin/load-files/index'])) ? "start active open" : "" ?>">
                <a href="<?= Url::to(['/admin/load-files/index', ]) ?>" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Загрузка файлов</span>
                    <?php if('/'.Yii::$app->request->pathInfo == Url::to(['/admin/load-files/index'])): ?>
                        <span class="selected"></span>
                    <?php endif; ?>
                </a>
            </li>
            <li class="nav-item <?= ('/'.Yii::$app->request->pathInfo == Url::to(['promotions/index'])) ? "start active open" : "" ?>">
                <a href="<?= Url::to(['promotions/index', ]) ?>" class="nav-link nav-toggle">
                    <i class="icon-bulb"></i>
                    <span class="title">Акции</span>
                    <?php if('/'.Yii::$app->request->pathInfo == Url::to(['promotions/index'])): ?>
                        <span class="selected"></span>
                    <?php endif; ?>
                </a>
            </li>
            <li class="nav-item <?= ('/'.Yii::$app->request->pathInfo == Url::to(['setting/index'])) ? "start active open" : "" ?>">
                <a href="<?= Url::to(['setting/index', ]) ?>" class="nav-link nav-toggle">
                    <i class="icon-wrench"></i>
                    <span class="title">Настройки</span>
                    <?php if('/'.Yii::$app->request->pathInfo == Url::to(['setting/index'])): ?>
                        <span class="selected"></span>
                    <?php endif; ?>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>