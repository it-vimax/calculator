<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 28.11.2017
 * Time: 18:17
 */
namespace app\modules\admin\widgets;

use yii\base\Widget;

class MenuWidget extends Widget
{
    public function run()
    {
        return $this->render('menu');
    }
}