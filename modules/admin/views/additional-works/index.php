<div class="row">
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Цена м пог откоса грн
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="slopes" type="text" class="form-control" value="<?php foreach($additionalWorks as $additionalWork)
                                                                                        {
                                                                                            if($additionalWork->name == "slopes")
                                                                                            {
                                                                                                echo $additionalWork->value;
                                                                                            }
                                                                                        }?>" placeholder="грн" />
                        <div class="slopes-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" data-name="slopes" id="btn-slopes">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Цена м квадратного монтажа грн
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="windowInstalation" type="text" class="form-control" value="<?php foreach($additionalWorks as $additionalWork)
                                                                                        {
                                                                                            if($additionalWork->name == "windowInstalation")
                                                                                            {
                                                                                                echo $additionalWork->value;
                                                                                            }
                                                                                        }?>" placeholder="грн" />
                        <div class="windowInstalation-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" data-name="windowInstalation" id="btn-windowInstalation">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Цена 1 москитной сетки грн
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="mosquitoNet" type="text" class="form-control" value="<?php foreach($additionalWorks as $additionalWork)
                                                                                        {
                                                                                            if($additionalWork->name == "mosquitoNet")
                                                                                            {
                                                                                                echo $additionalWork->value;
                                                                                            }
                                                                                        }?>" placeholder="грн" />
                        <div class="mosquitoNet-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" data-name="mosquitoNet" id="btn-mosquitoNet">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Цена вывоза мусора грн
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="gorbageRemoval" type="gorbageRemoval" class="form-control" value="<?php foreach($additionalWorks as $additionalWork)
                                                                                        {
                                                                                            if($additionalWork->name == "gorbageRemoval")
                                                                                            {
                                                                                                echo $additionalWork->value;
                                                                                            }
                                                                                        }?>" placeholder="грн" />
                        <div class="gorbageRemoval-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" data-name="gorbageRemoval" id="btn-gorbageRemoval">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>
