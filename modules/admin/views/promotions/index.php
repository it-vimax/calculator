<div class="row">
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Общая рекламная скидка %
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="generalAdvertisingDiscount" type="text" class="form-control" value="<?php foreach($promotions as $promotion)
                                                                                        {
                                                                                            if($promotion->name == "generalAdvertisingDiscount")
                                                                                            {
                                                                                                echo $promotion->value;
                                                                                            }
                                                                                        }?>" placeholder="%" />
                        <div class="generalAdvertisingDiscount-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" id="btn-generalAdvertisingDiscount-change">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Скидка на окна %
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="discountOnWindows" type="text" class="form-control" value="<?php foreach($promotions as $promotion)
                                                                                        {
                                                                                            if($promotion->name == "discountOnWindows")
                                                                                            {
                                                                                                echo $promotion->value;
                                                                                            }
                                                                                        }?>" placeholder="%" />
                        <div class="discountOnWindows-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" id="btn-discountOnWindows-change">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>