<?php use yii\bootstrap\ActiveForm; ?>

<div class="row">
    <div class="col-md-12">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-12">
                    <p>Соблюдайте правила найменования файлов, это влияет на работу калькулятора!</p>
                    <p>
                        <b>Таблицы подписываются</b>: 2gp,b58,ax,w,32i.<br>
                        <b>Типы окон</b>: 1g - глухое, 1p - поворотно-откидное, 2gp - гл/пов-отк, 3gpf - гл/пов-отк/фрамуга, 3gpg - гл/пов-отк/гл, 3pgp - пов-отк/гл/пов-отк, 4gppg - гл/пов-отк/пов-отк/гл, 1b - балконная дверь.<br>
                        <b>Профиля</b>: b58 - викналенд в58, b70 - викналенд в70, al4 - алюпласт 4000, lid - лидер.<br>
                        <b>Стеклопакет</b>: 24 - однокамерный, 32i - двухкамерный энерго, 32mf - двухкамерный мультифункционый.<br>
                        <b>Фурнитура</b>: ax - Аксор, sg - Зигения.<br>
                        <b>Цвет</b>: w - белый, c - цветной.<br>
                    </p>
                    <div id="field-excel-load">
                        <input type="file" name="file-excel-load" class="btn btn-success mt-sweetalert" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        <div class="file-excel-message-field"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="col-md-6">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-bell-o"></i>Загруженые файлы </div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"> </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table id="table-excel" class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                <i class="fa fa-file-excel-o"></i> #
                                            </th>
                                            <th>
                                                <i class="fa fa-file-excel-o"></i> Название файла
                                            </th>
                                            <th>
                                                Росширение
                                            </th>
                                            <th> </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(!empty($filesAll)): ?>
                                            <?php foreach($filesAll as $key => $file): ?>
                                                <tr>
                                                    <td class="highlight">
                                                        <div class="success"></div>
                                                        <a href="javascript:;"> <?= $key + 1 ?> </a>
                                                    </td>
                                                    <td class="highlight">
                                                        <a href="javascript:;"> <?= $file->name ?> </a>
                                                    </td>
                                                    <td class="highlight">
                                                        <a href="javascript:;"> <?= $file->extension ?> </a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:;" data-id="<?= $file->id ?>" btn-id="btn-excel-delete" class="btn btn-outline btn-circle dark btn-sm black">
                                                            <i class="fa fa-trash-o"></i> Удалить </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <? else: ?>
                                            <tr>
                                                <td class="highlight">
                                                    Пустая таблица
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


