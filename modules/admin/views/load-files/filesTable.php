<?php if(!empty($filesAll)): ?>
    <?php foreach($filesAll as $key => $file): ?>
        <tr>
            <td class="highlight">
                <div class="success"></div>
                <a href="javascript:;"> <?= $key + 1 ?> </a>
            </td>
            <td class="highlight">
                <div class="success"></div>
                <a href="javascript:;"> <?= $file->name ?> </a>
            </td>
            <td class="highlight">
                <a href="javascript:;"> <?= $file->extension ?> </a>
            </td>
            <td>
                <a href="javascript:;" data-id="<?= $file->id ?>" btn-id="btn-excel-delete" class="btn btn-outline btn-circle dark btn-sm black">
                    <i class="fa fa-trash-o"></i> Удалить </a>
            </td>
        </tr>
    <?php endforeach; ?>
<? else: ?>
    <tr>
        <td class="highlight">
            Пустая таблица
        </td>
    </tr>
<?php endif; ?>