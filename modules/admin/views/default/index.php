<?php
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    <p>
                        На эти адреса будут отправлятся присьма при создании заказа клиентом.
                    </p>
                    Добавить Email
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="title" name="email" type="email" class="form-control" placeholder="email" />
                        <div class="email-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" id="email-add">Добавить</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    <p>
                        Эти номера будут отображатся при создании заказа клиентом.
                    </p>
                    Добавить намер телефона
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input id="title" name="phone" type="text" class="form-control" placeholder="+380(00) 000-00-00" />
                        <div class="phone-message-field"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" id="phone-add">Добавить</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">

            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>Список Email</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover" id="email-table">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-paperclip"></i> Email </th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($emails)): ?>
                                <?php foreach($emails as $email): ?>
                                    <tr>
                                        <td class="highlight">
                                            <div class="success"></div>
                                            <a href="javascript:;"> <?= $email->value ?> </a>
                                        </td>
                                        <td>
                                            <a href="javascript:;" data-id="<?= $email->id ?>" id="btn-email-delete" class="btn btn-outline btn-circle dark btn-sm black">
                                                <i class="fa fa-trash-o"></i> Удалить </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td>Список Email пуст</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">

            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>Список номеров телефона</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover" id="phone-table">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-paperclip"></i> Номер телефона </th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($phones)): ?>
                                <?php foreach($phones as $phone): ?>
                                    <tr>
                                        <td class="highlight">
                                            <div class="success"></div>
                                            <a href="javascript:;"> <?= $phone->value ?> </a>
                                        </td>
                                        <td>
                                            <a href="javascript:;" data-id="<?= $phone->id ?>" id="btn-phone-delete" class="btn btn-outline btn-circle dark btn-sm black">
                                                <i class="fa fa-trash-o"></i> Удалить </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td>Список номерав пуст</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>



