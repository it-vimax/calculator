<?php if(!empty($phones)): ?>
    <?php foreach($phones as $phone): ?>
        <tr>
            <td class="highlight">
                <div class="success"></div>
                <a href="javascript:;"> <?= $phone->value ?> </a>
            </td>
            <td>
                <a href="javascript:;" data-id="<?= $phone->id ?>" id="btn-phone-delete" class="btn btn-outline btn-circle dark btn-sm black">
                    <i class="fa fa-trash-o"></i> Удалить </a>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td>Список номерав пуст</td>
    </tr>
<?php endif; ?>