<?php if(!empty($emails)): ?>
    <?php foreach($emails as $email): ?>
        <tr>
            <td class="highlight">
                <div class="success"></div>
                <a href="javascript:;"> <?= $email->value ?> </a>
            </td>
            <td>
                <a href="javascript:;" data-id="<?= $email->id ?>" id="btn-email-delete" class="btn btn-outline btn-circle dark btn-sm black">
                    <i class="fa fa-trash-o"></i> Удалить </a>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td>Список Email пуст</td>
    </tr>
<?php endif; ?>

