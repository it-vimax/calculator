
<?php if(!empty($outflows)): ?>
    <?php foreach($outflows as $outflow): ?>
        <tr>
            <td class="highlight">
                <div class="success"></div>
                <a href="javascript:;"> <?= $outflow->depth ?> </a>
            </td>
            <td> <?= $outflow->color ?> </td>
            <td> <?= $outflow->manufacturer ?> </td>
            <td> <?= $outflow->price ?> </td>
            <td>
                <a href="javascript:;" data-id="<?= $outflow->id ?>" id="btn-outflows-delete" class="btn btn-outline btn-circle dark btn-sm black">
                    <i class="fa fa-trash-o"></i> Удалить </a>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td> Нет доступных отливов </td>
    </tr>
<?php endif; ?>