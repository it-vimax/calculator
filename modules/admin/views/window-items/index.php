<div class="row">
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Подоконник стомиость за 1 м/п
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input name="sill-new-depth" type="text" class="form-control" value="" placeholder="клубина в мм" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input name="sill-new-price" type="text" class="form-control" value="" placeholder="грн" />
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" id="btn-sills-add">Сохранить</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="sill-message-field"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">
            <div class="row">
                <div class="col-md-6">
                    Отлив стомиость за 1 м/п
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input name="outflows-new-depth" type="text" class="form-control" value="" placeholder="клубина в мм" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input name="outflows-new-price" type="text" class="form-control" value="" placeholder="грн" />
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn green" id="btn-outflows-add">Сохранить</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="outflows-message-field"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">

            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>Подоконники</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover" id="sill-table">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-arrows-h"></i> Глубина </th>
                                <th>
                                    <i class="fa fa-adjust"></i> Цвет </th>
                                <th>
                                    <i class="fa fa-legal"></i> Производитель </th>
                                <th>
                                    <i class="fa fa-shopping-cart"></i> Цена в грн </th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($sills)): ?>
                                <?php foreach($sills as $sill): ?>
                                    <tr>
                                        <td class="highlight">
                                            <div class="success"></div>
                                            <a href="javascript:;"> <?= $sill->depth ?> </a>
                                        </td>
                                        <td> <?= $sill->color ?> </td>
                                        <td> <?= $sill->manufacturer ?> </td>
                                        <td> <?= $sill->price ?> </td>
                                        <td>
                                            <a href="javascript:;" data-id="<?= $sill->id ?>" id="btn-sill-delete" class="btn btn-outline btn-circle dark btn-sm black">
                                                <i class="fa fa-trash-o"></i> Удалить </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td> Нет доступных подокоников </td>
                                </tr>
                            <?php endif; ?>
  
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->



        </div>
    </div>
    <div class="col-md-6">
        <div class="m-heading-1 border-green m-bordered">


            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>Отливы</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table id="outflow-table" class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-arrows-h"></i> Клубина </th>
                                <th>
                                    <i class="fa fa-adjust"></i> Цвет </th>
                                <th>
                                    <i class="fa fa-legal"></i> Производитель </th>
                                <th>
                                    <i class="fa fa-shopping-cart"></i> Цена в грн </th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php if(!empty($outflows)): ?>
                                <?php foreach($outflows as $outflow): ?>
                                    <tr>
                                        <td class="highlight">
                                            <div class="success"></div>
                                            <a href="javascript:;"> <?= $outflow->depth ?> </a>
                                        </td>
                                        <td> <?= $outflow->color ?> </td>
                                        <td> <?= $sill->manufacturer ?> </td>
                                        <td> <?= $outflow->price ?> </td>
                                        <td>
                                            <a href="javascript:;" data-id="<?= $outflow->id ?>" id="btn-outflows-delete" class="btn btn-outline btn-circle dark btn-sm black">
                                                <i class="fa fa-trash-o"></i> Удалить </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td> Нет доступных отливов </td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->


        </div>
    </div>
</div>