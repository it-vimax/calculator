<?php if(!empty($sills)): ?>
    <?php foreach($sills as $sill): ?>
        <tr>
            <td class="highlight">
                <div class="success"></div>
                <a href="javascript:;"> <?= $sill->depth ?> </a>
            </td>
            <td> <?= $sill->color ?> </td>
            <td> <?= $sill->manufacturer ?> </td>
            <td> <?= $sill->price ?> </td>
            <td>
                <a href="javascript:;" data-id="<?= $sill->id ?>" id="btn-sill-delete" class="btn btn-outline btn-circle dark btn-sm black">
                    <i class="fa fa-trash-o"></i> Delete </a>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td> Нет доступных подокоников </td>
    </tr>
<?php endif; ?>