<?php

namespace app\modules\admin\controllers;

use app\models\Files;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class LoadFilesController extends AppController
{
    public function actionIndex()
    {
        $filesAll = Files::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', compact('filesAll'));
    }

    public function beforeAction($action)
    {
        if ($action->id == 'my-method') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionSaveExcel()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.file-excel-message-field';
            if(!empty($_FILES))
            {
                $basePath = Url::to('@app');
                $uploadDir = $basePath.'/excel/';
                $file = basename($_FILES[0]['name']);
                $fielAttr = explode(".", $file);
                $fileFind = Files::find()->where(['name' => $fielAttr[0]])->limit(1)->all()[0];
                if(!isset($fileFind))
                {
                    $load = move_uploaded_file($_FILES[0]['tmp_name'], $uploadDir . $file);
                    if($load)
                    {
                        $fileSave = new Files();
                        $fileSave->name = (string)$fielAttr[0];
                        $fileSave->extension = (string) '.'.$fielAttr[1];
                        $fileSave->path = '@app/excel/';
                        $fileSave->save();
                        $this->messageType = 'success';
                        $this->messageStr = "Файл {$fielAttr[0]} удачно загружен на сервер!";
                        $filesAll = Files::find()->orderBy(['id' => SORT_DESC])->all();
                        $this->showTabe = $this->renderAjax("filesTable", compact('filesAll'));
                        return $this->sendJson();
                    }
                    else
                    {
                        $this->messageStr = "Не удалось загрузить файл {$fielAttr[0]} на сервер";
                        $this->messageType = 'danger';
                        return $this->sendJson();
                    }
                }
                else
                {
                    $this->messageStr = "Файл {$fielAttr[0]} уже существует";
                    $this->messageType = 'warning';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Не передаем файл";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionDeleteExcel()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.file-excel-message-field';
            $id = Yii::$app->request->post('delete');
            if(!empty($id))
            {
                $file = Files::find()->where(['id' => $id])->limit(1)->all()[0];
                if($file)
                {
                    unlink(Url::to($file->path . $file->name . $file->extension));
                    Yii::$app->db->createCommand()->delete('files', ['id' => $id])->execute();
                    $filesAll = Files::find()->orderBy(['id' => SORT_DESC])->all();
                    $this->messageStr = "Файл \"{$file->name}\" успешно удален!";
                    $this->showTabe = $this->renderAjax("filesTable", compact('filesAll'));
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Файл не найден";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пустое значение id file";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }
}