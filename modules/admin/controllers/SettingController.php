<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 28.11.2017
 * Time: 23:36
 */

namespace app\modules\admin\controllers;


use app\models\Settings;
use Yii;
use yii\web\Controller;

class SettingController extends AppController
{
    public function actionIndex()
    {
        $settings = Settings::find()->all();
        return $this->render('index', compact('settings'));
    }

    public function actionDollarExchangeRateChange()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.dollarExchangeRate-message-field';
            $data = Yii::$app->request->post('dollarExchangeRate');
            if(!empty($data))
            {
                $dollars = Settings::find()->where(['name' => 'dollarExchangeRate'])->one();
                if(empty($dollars))
                {
                    $dollars = new Settings();
                    $dollars->name = 'dollarExchangeRate';
                }
                $dollars->value = $data;
                if($dollars->save() == true)
                {
                    $this->messageStr = "Цена $data за 1 доллар США успешно изменена!";
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Значение = \"$data\" не может быть ценой";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Не удальсь изменить курс доллара";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionCacheUpdate()
    {
        if(Yii::$app->request->isPost)
        {
            Yii::$app->cache->flush();
            $this->nameField = '.cache-message-field';
            $this->messageStr = "Кеш очещен";
            $this->messageType = 'success';
            return $this->sendJson();
        }
        return $this->redirect('/');
    }
}