<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 28.11.2017
 * Time: 1:55
 */

namespace app\modules\admin\controllers;

use app\models\Outflows;
use app\models\Sills;
use Yii;
use yii\web\Controller;

class WindowItemsController extends AppController
{
    public function actionIndex()
    {
        $sills = Sills::find()->orderBy(['id' => SORT_DESC])->all();
        $outflows = Outflows::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', compact('sills', 'outflows'));
    }

    public function actionOutflowsAdd()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = ".outflows-message-field";
            $outflowDepth = Yii::$app->request->post('outflowDepth');
            $outflowPrice = Yii::$app->request->post('outflowPrice');
            if(!empty($outflowDepth) && !empty($outflowPrice))
            {
                $outflow = new Outflows();
                $outflow->depth = $outflowDepth;
                $outflow->price = $outflowPrice;
                $outflow->color = "Белый";
                $outflow->manufacturer = "Украина";
                if($outflow->save())
                {
                    $this->messageStr = "Глубина отлива {$outflow->depth} и его цена {$outflow->price} успешно изменена!";
                    $outflows = Outflows::find()->orderBy(['id' => SORT_DESC])->all();
                    $this->showTabe = $this->renderAjax('outflowTable', compact('outflows'));
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Не коректное значение цены или глубины отлива";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пустое значение компонентов стоимости или размеров отлива";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionOutflowsDelete()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = ".outflows-message-field";
            $id = Yii::$app->request->post('delete');
            if(!empty($id))
            {
                Yii::$app->db->createCommand()->delete('outflows', ['id' => $id])->execute();
                $outflows = Outflows::find()->orderBy(['id' => SORT_DESC])->all();
                $this->showTabe = $this->renderAjax('outflowTable', compact('outflows'));
                $this->messageStr = "Отлив успешно удален!";
                $this->messageType = 'success';
                return $this->sendJson();
            }
            else
            {
                return $this->asJson($error[] = ['Пустое значение OutflowsDelete']);
            }
        }
        return $this->redirect('/');
    }

    public function actionSillAdd()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = ".sill-message-field";
            $sillDepth = Yii::$app->request->post('sillDepth');
            $sillPrice = Yii::$app->request->post('sillPrice');
            if(!empty($sillDepth) && !empty($sillPrice))
            {
                $sill = new Sills();
                $sill->depth = $sillDepth;
                $sill->price = $sillPrice;
                $sill->color = "Белый";
                $sill->manufacturer = "Украина";
                if($sill->save())
                {
                    $this->messageStr = "Ширина подоконника {$sill->depth} и его цена {$sill->price} успешно изменена!";
                    $this->messageType = 'success';
                    $sills = Sills::find()->orderBy(['id' => SORT_DESC])->all();
                    $this->showTabe = $this->renderAjax('sillTable', compact('sills'));
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Не коректное значение цены или ширины подоконника";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пустое значение компонентов стоимости или размеров подоконника";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionSillDelete()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = ".sill-message-field";
            $id = Yii::$app->request->post('delete');
            if(!empty($id))
            {
                Yii::$app->db->createCommand()->delete('sills', ['id' => $id])->execute();
                $sills = Sills::find()->orderBy(['id' => SORT_DESC])->all();
                $this->showTabe = $this->renderAjax('sillTable', compact('sills'));
                $this->messageStr = "Подоконник успешно удален!";
                $this->messageType = 'success';
                return $this->sendJson();
            }
            else
            {
                return $this->asJson($error[] = ['Пустое значение SillDelete']);
            }
        }
        return $this->redirect('/');
    }
}