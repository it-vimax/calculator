<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 02.12.2017
 * Time: 9:20
 */

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class AppController extends Controller
{
    public $pathViewMessage = '/components/message.php';
    public $messageStr;
    public $showTabe;
    public $messageType;
    public $message;
    public $nameField = '.message-field';
    public $response;

    public function sendJson()
    {
        $this->message = $this->renderFile(Url::to('@app') . $this->pathViewMessage, [
            'messageStr' => $this->messageStr,
            'messageType' => $this->messageType,
        ]);
        $dataArr = [];
        if(!empty($this->showTabe))
        {
            $dataArr['showTable'] = $this->showTabe;
        }
        if(!empty($this->response))
        {
            $dataArr['response'] = $this->response;
        }
        $dataArr['messageType'] = $this->messageType;
        $dataArr['message'] = $this->message;
        $dataArr['nameField'] = $this->nameField;

        return $this->asJson($dataArr);
    }
}