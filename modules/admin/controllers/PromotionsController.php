<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 28.11.2017
 * Time: 23:31
 */

namespace app\modules\admin\controllers;


use app\models\AdditionalWorks;
use app\models\Promotions;
use Yii;
use yii\web\Controller;

class PromotionsController extends AppController
{
    public function actionIndex()
    {
        $promotions = Promotions::find()->all();
        return $this->render('index', compact('promotions'));
    }
    
    public function actionDiscountOnWindowsChange()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.discountOnWindows-message-field';
            $data = Yii::$app->request->post('discountOnWindows');
            $promotions = Promotions::find()->where(['name' => 'discountOnWindows'])->one();
            if(empty($promotions))
            {
                $promotions = new Promotions();
                $promotions->name = 'discountOnWindows';
            }
            $promotions->value = $data;
            if($promotions->save())
            {
                $this->messageStr = "Скидка $data на окна успешно изменена!";
                $this->messageType = 'success';
                return $this->sendJson();
            }
            else
            {
                $this->messageStr = "Значение = \"$data\" не может быть скидкой";
                $this->messageType = 'danger';
                return $this->sendJson();
            }

        }
        return $this->redirect('/');
    }

    public function actionGeneralAdvertisingDiscountChange()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.generalAdvertisingDiscount-message-field';
            $data = Yii::$app->request->post('generalAdvertisingDiscount');
            $promotions = Promotions::find()->where(['name' => 'generalAdvertisingDiscount'])->one();
            if(empty($promotions))
            {
                $promotions = new Promotions();
                $promotions->name = 'generalAdvertisingDiscount';
            }
            $promotions->value = $data;
            if($promotions->save())
            {
                $this->messageStr = "Общая рекламная скидка  $data успешно изменена!";
                $this->messageType = 'success';
                return $this->sendJson();
            }
            else
            {
                $this->messageStr = "Значение = \"$data\" не может быть общей рекламной скидкой";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }
}