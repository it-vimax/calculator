<?php

namespace app\modules\admin\controllers;

use app\models\Emails;
use app\models\Phones;
use mdm\admin\models\User;
use Yii;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends AppController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $emails = Emails::find()->orderBy(['id' => SORT_DESC])->all();
        $phones = Phones::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('index', compact('emails', 'phones'));
    }

    public function actionPhoneAdd()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.phone-message-field';
            $newPhone = Yii::$app->request->post('phone');
            if(!empty($newPhone))
            {
                $findPhone = Phones::find()->where(['value' => $newPhone])->one();
                if(!$findPhone)
                {
                    $phone = new Phones();
                    $phone->value = $newPhone;
                    if($phone->save())
                    {
                        $phones = Phones::find()->orderBy(['id' => SORT_DESC])->all();
                        $this->messageStr = "Номер телефона \"{$phone->value}\" успешно добавлен!";
                        $this->showTabe = $this->renderAjax('phoneTable', compact('phones'));
                        $this->messageType = 'success';
                        return $this->sendJson();
                    }
                    else
                    {
                        $this->messageStr = "Строка \"{$phone->value}\" не является номером телефона";
                        $this->messageType = 'danger';
                        return $this->sendJson();
                    }
                }
                else
                {
                    $this->messageStr = "Номер телефона \"{$findPhone->value}\" уже существует";
                    $this->messageType = 'warning';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Поле номера телефона не может быть пустым";
                $this->messageType = 'warning';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionPhoneDelete()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.phone-message-field';
            $id = Yii::$app->request->post('delete');
            if(!empty($id))
            {
                $phone = Phones::find()->where(['id' => $id])->one();
                if($phone)
                {
                    Yii::$app->db->createCommand()->delete('phones', ['id' => $id])->execute();
                    $phones = Phones::find()->orderBy(['id' => SORT_DESC])->all();
                    $this->messageStr = "Номер телефона \"{$phone->value}\" успешно удален!";
                    $this->showTabe = $this->renderAjax('phoneTable', compact('phones'));
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Номер телефона \"{$phone->value}\" не найден";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пришло пустое занчеие";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionEmailAdd()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.email-message-field';
            $newEmail = Yii::$app->request->post('email');
            if(!empty($newEmail))
            {
                $findEmail = Emails::find()->where(['value' => $newEmail])->one();
                if(!$findEmail)
                {
                    $email = new Emails();
                    $email->value = $newEmail;
                    if($email->save())
                    {
                        $emails = Emails::find()->orderBy(['id' => SORT_DESC])->all();
                        $this->messageStr = "Email \"{$email->value}\" успешно добавлен!";
                        $this->showTabe = $this->renderAjax('emailTable', compact('emails'));
                        $this->messageType = 'success';
                        return $this->sendJson();
                    }
                    else
                    {
                        $this->messageStr = "Строка \"{$email->value}\" не является адрессом электронной почты";
                        $this->messageType = 'danger';
                        return $this->sendJson();
                    }
                }
                else
                {
                    $this->messageStr = "Email \"{$findEmail->value}\" уже существует";
                    $this->messageType = 'warning';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Поле email не может быть пустым";
                $this->messageType = 'warning';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionEmailDelete()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.email-message-field';
            $id = Yii::$app->request->post('delete');
            if(!empty($id))
            {
                $email = Emails::find()->where(['id' => $id])->one();
                if($email)
                {
                    Yii::$app->db->createCommand()->delete('emails', ['id' => $id])->execute();
                    $emails = Emails::find()->orderBy(['id' => SORT_DESC])->all();
                    $this->messageStr = "Email \"{$email->value}\" успешно удален!";
                    $this->showTabe = $this->renderAjax('emailTable', compact('emails'));
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Email \"{$email->value}\" не найден";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пришло пустое занчеие";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }
}
