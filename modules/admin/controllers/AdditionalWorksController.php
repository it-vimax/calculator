<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 28.11.2017
 * Time: 23:22
 */

namespace app\modules\admin\controllers;


use app\models\AdditionalWorks;
use Yii;
use yii\web\Controller;

class AdditionalWorksController extends AppController
{
    public function actionIndex()
    {
        $additionalWorks = AdditionalWorks::find()->all();
        return $this->render('index', compact('additionalWorks'));
    }
    
    public function actionGorbageRemovalChange()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.gorbageRemoval-message-field';
            $data = Yii::$app->request->post('gorbageRemoval');
            if(!empty($data))
            {
                $gorbageRemoval = AdditionalWorks::find()->where(['name' => 'gorbageRemoval'])->one();
                if(empty($gorbageRemoval))
                {
                    $gorbageRemoval = new AdditionalWorks();
                    $gorbageRemoval->name = 'gorbageRemoval';
                }
                $gorbageRemoval->value = $data;
                if($gorbageRemoval->save())
                {
                    $this->messageStr = "Цена $data за 1 вывоз мусора успешно изменена!";
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Значение = \"$data\" не может быть ценой";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пустое значение стоимочти вывоза мусора";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionMosquitoNetChange()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.mosquitoNet-message-field';
            $data = Yii::$app->request->post('mosquitoNet');
            if(!empty($data))
            {
                $mosquitoNet = AdditionalWorks::find()->where(['name' => 'mosquitoNet'])->one();
                if(empty($mosquitoNet))
                {
                    $mosquitoNet = new AdditionalWorks();
                    $mosquitoNet->name = 'mosquitoNet';
                }
                $mosquitoNet->value = $data;
                if($mosquitoNet->save())
                {
                    $this->messageStr = "Цена $data за 1 москитную сетку успешно изменена!";
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Значение = \"$data\" не может быть ценой";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пустое значение стоимочти москитной сетки";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionWindowInstalationChange()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.windowInstalation-message-field';
            $data = Yii::$app->request->post('windowInstalation');
            if(!empty($data))
            {
                $windowInstalation = AdditionalWorks::find()->where(['name' => 'windowInstalation'])->one();
                if(empty($windowInstalation))
                {
                    $windowInstalation = new AdditionalWorks();
                    $windowInstalation->name = 'windowInstalation';
                }
                $windowInstalation->value = $data;
                if($windowInstalation->save())
                {
                    $this->messageStr = "Цена $data монтажа успешно изменена!";
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Значение = \"$data\" не может быть ценой";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пустое значение стоимочти монтажа";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }

    public function actionSlopesChange()
    {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost)
        {
            $this->nameField = '.slopes-message-field';
            $data = Yii::$app->request->post('slopes');
            if(!empty($data))
            {
                $slopes = AdditionalWorks::find()->where(['name' => 'slopes'])->one();
                if(empty($slopes))
                {
                    $slopes = new AdditionalWorks();
                    $slopes->name = 'slopes';
                }
                $slopes->value = $data;
                if($slopes->save())
                {
                    $this->messageStr = "Цена $data за м/п откоса успешно изменена!";
                    $this->messageType = 'success';
                    return $this->sendJson();
                }
                else
                {
                    $this->messageStr = "Значение = \"$data\" не может быть ценой";
                    $this->messageType = 'danger';
                    return $this->sendJson();
                }
            }
            else
            {
                $this->messageStr = "Пустое значение стоимочти откоса";
                $this->messageType = 'danger';
                return $this->sendJson();
            }
        }
        return $this->redirect('/');
    }
}