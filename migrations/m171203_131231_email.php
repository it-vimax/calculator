<?php

use yii\db\Migration;

/**
 * Class m171203_131231_email
 */
class m171203_131231_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('emails', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('emails');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171203_131231_email cannot be reverted.\n";

        return false;
    }
    */
}
