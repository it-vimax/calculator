<?php

use yii\db\Migration;

/**
 * Class m171129_162016_Outflows
 */
class m171129_162016_Outflows extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('outflows', [
            'id' => $this->primaryKey(),
            'depth' => $this->integer(),
            'manufacturer' => $this->string(),
            'color' => $this->string(),
            'price' => $this->decimal(10, 2),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('outflows');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171129_162016_Outflows cannot be reverted.\n";

        return false;
    }
    */
}
