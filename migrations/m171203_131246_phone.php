<?php

use yii\db\Migration;

/**
 * Class m171203_131246_hpone
 */
class m171203_131246_phone extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('phones', [
            'id' => $this->primaryKey(),
            'value' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('phones');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171203_131246_hpone cannot be reverted.\n";

        return false;
    }
    */
}
