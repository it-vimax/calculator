<?php

use yii\db\Migration;

/**
 * Class m171129_141116_Sills
 */
class m171129_141116_Sills extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('sills', [
            'id' => $this->primaryKey(),
            'depth' => $this->integer(),
            'manufacturer' => $this->string(),
            'color' => $this->string(),
            'price' => $this->decimal(10, 2),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('sills');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171129_141116_Sills cannot be reverted.\n";

        return false;
    }
    */
}
