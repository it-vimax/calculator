<?php

use yii\db\Migration;

/**
 * Class m171130_113946_Promotions
 */
class m171130_113946_Promotions extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('promotions', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'value' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('promotions');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_113946_Promotions cannot be reverted.\n";

        return false;
    }
    */
}
