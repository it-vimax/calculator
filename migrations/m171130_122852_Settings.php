<?php

use yii\db\Migration;

/**
 * Class m171130_122852_Settings
 */
class m171130_122852_Settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'value' => $this->decimal(10,2),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_122852_Settings cannot be reverted.\n";

        return false;
    }
    */
}
