<?php

use yii\db\Migration;

/**
 * Class m171130_131642_Files
 */
class m171130_131642_Files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'extension' => $this->string(),
            'path' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('files');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_131642_Files cannot be reverted.\n";

        return false;
    }
    */
}
