<?php

use yii\db\Migration;

/**
 * Class m171130_085932_AdditionalWorks
 */
class m171130_085932_AdditionalWorks extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('additional_works', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'value' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('additional_works');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_085932_AdditionalWorks cannot be reverted.\n";

        return false;
    }
    */
}
