<h3>Оформление заказа с калькулятора</h3>
Имя: <?= $client['name'] ?><br>
Почта: <?= $client['email'] ?><br>
Номер телефона: <?= $client['phone'] ?><br>
Вопрос: <?= $client['yourQuestion'] ?><br>
<?php if(!empty($client['other_consultation'])): ?>
    Нужна друкая конструкция!<br>
<?php else: ?>
    <br>
    <?php if(isset($_SESSION['totalPrice'])): ?>
        Характеристики заказа:<br>

        <table>
            <tr>
                <td><b>Тип окна</b></td>
                <td><?= $_SESSION['totalPrice']['window']['type'] ?></td>
            </tr>
            <tr>
                <td><b>Ширина окна</b></td>
                <td><?= $_SESSION['totalPrice']['window']['width'] ?></td>
            </tr>
            <tr>
                <td><b>Высота окна</b></td>
                <td><?= $_SESSION['totalPrice']['window']['hight'] ?></td>
            </tr>
            <?php if(isset($_SESSION['totalPrice']['dor']['width'])): ?>
                <tr>
                    <td><b>Ширина двери</b></td>
                    <td><?= $_SESSION['totalPrice']['dor']['width'] ?></td>
                </tr>
                <tr>
                    <td><b>Высота двери</b></td>
                    <td><?= $_SESSION['totalPrice']['dor']['hight'] ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td><b>Цена окна</b></td>
                <td><?= $_SESSION['totalPrice']['window']['price'] ?></td>
            </tr>
            <tr>
                <td><b>Профиль окна</b></td>
                <td><?= $_SESSION['totalPrice']['window']['profile'] ?></td>
            </tr>
            <tr>
                <td><b>Стеклопакет</b></td>
                <td><?= $_SESSION['totalPrice']['window']['steklopaket'] ?></td>
            </tr>
            <?php if(isset($_SESSION['totalPrice']['window']['furnitura'])): ?>
                <tr>
                    <td><b>Фурнитура окна</b></td>
                    <td><?= $_SESSION['totalPrice']['window']['furnitura'] ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td><b>Цвет</b></td>
                <td><?= $_SESSION['totalPrice']['color'] == 'w' ? 'белый' : 'цветной' ?></td>
            </tr>
            <?php if(isset($_SESSION['totalPrice']['otliv']['size'])): ?>
                <tr>
                    <td><b>Отлив размер</b></td>
                    <td><?= $_SESSION['totalPrice']['otliv']['size'] ?></td>
                </tr>
            <?php endif; ?>
            <?php if(isset($_SESSION['totalPrice']['otliv']['price'])): ?>
                <tr>
                    <td><b>Отлив цена</b></td>
                    <td><?= $_SESSION['totalPrice']['otliv']['price'] ?></td>
                </tr>
            <?php endif; ?>
            <?php if(isset($_SESSION['totalPrice']['podokonnik']['price'])): ?>
                <tr>
                    <td><b>Подоконник размер</b></td>
                    <td><?= $_SESSION['totalPrice']['podokonnik']['size'] ?></td>
                </tr>
                <tr>
                    <td><b>Подоконник цена</b></td>
                    <td><?= $_SESSION['totalPrice']['podokonnik']['price'] ?></td>
                </tr>
            <?php endif; ?>
            <?php if(isset($_SESSION['totalPrice']['otkosi']['price'])): ?>
                <tr>
                    <td><b>Откосы размер</b></td>
                    <td><?= $_SESSION['totalPrice']['otkosi']['size'] ?></td>
                </tr>
                <tr>
                    <td><b>Откосы цена</b></td>
                    <td><?= $_SESSION['totalPrice']['otkosi']['price'] ?></td>
                </tr>
            <?php endif; ?>
            <?php if(isset($_SESSION['totalPrice']['montazh'])): ?>
                <tr>
                    <td><b>Монтаж</b></td>
                    <td><?=  $_SESSION['totalPrice']['montazh'] ?></td>
                </tr>
            <?php endif; ?>
            <?php if(isset($_SESSION['totalPrice']['moskitnai_setka'])): ?>
                <tr>
                    <td><b>Москитная сетка</b></td>
                    <td><?= $_SESSION['totalPrice']['moskitnai_setka'] ?></td>
                </tr>
            <?php endif; ?>
            <?php if(isset($_SESSION['totalPrice']['vivoz_musora'])): ?>
                <tr>
                    <td><b>Вывоз мусора</b></td>
                    <td><?= $_SESSION['totalPrice']['vivoz_musora'] ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td><b>Цена</b></td>
                <td><?= $_SESSION['totalPrice']['total_price'] ?></td>
            </tr>
            <tr>
                <td><b>Цена с <?= $_SESSION['totalPrice']['sale']['percent'] ?>% рекламной скидкой</b></td>
                <td><?= $_SESSION['totalPrice']['sale']['total'] ?></td>
            </tr>
        </table>
    <?php endif; ?>
<?php endif; ?>

