<?php
//
//use yii\helpers\Html;
//use yii\helpers\Url;
//?>
<!---->
<!--<section class="calculator_page">-->
<!--    <div class="calculator_wrapper">-->
<!--        <div class="calc_head">-->
<!--            <h1>Калькулятор окон</h1>-->
<!--            <p class="calc_steps_name">шаг 1: Выберите конструкцию</p>-->
<!--            <div class="calc_type_of_construction">-->
<!--                --><?//= Html::img('img/type_of_win1.png', ['id' => 'window__0']) ?>
<!--                --><?//= Html::img('img/type_of_win2.png', ['id' => 'window__1']) ?>
<!--                --><?//= Html::img('img/type_of_win3.png', ['id' => 'window__01']) ?>
<!--                --><?//= Html::img('img/type_of_win4.png', ['id' => 'window__0_1']) ?>
<!--                --><?//= Html::img('img/type_of_win5.png', ['id' => 'window__010']) ?>
<!--                --><?//= Html::img('img/type_of_win6.png', ['id' => 'window__101']) ?>
<!--                --><?//= Html::img('img/type_of_win7.png', ['id' => 'window__0110']) ?>
<!--                --><?//= Html::img('img/type_of_win8.png', ['id' => 'window__d0', 'class' => 'balcon_constr']) ?>
<!--                --><?//= Html::img('img/type_of_win9.png', ['id' => 'window__d01', 'class' => 'balcon_constr']) ?>
<!--            </div>-->
<!---->
<!--            <div class="need_other_construction">Нужна другая конструкция?</div>-->
<!--        </div>-->
<!---->
<!--        <div class="calculator_body">-->
<!--            <p class="calc_steps_name">шаг 2: Выберите основные комплектующие</p>-->
<!--            <div class="calc_characteristics_left">-->
<!--                <div class="calc_characteristics_left_item" id="profileVal">-->
<!--                    <span class="name_calc_left_item">Профиль</span>-->
<!--                    <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                    <ul class="notactive">-->
<!--                        <li data-value="lider">Lider</li>-->
<!--                        <li data-value="B58">B58</li>-->
<!--                        <li data-value="B70">B70</li>-->
<!--                    </ul>-->
<!---->
<!--                    <select name="profile" id="profile" class="notactive">-->
<!--                        <option value="lider">Lider</option>-->
<!--                        <option value="B58">B58</option>-->
<!--                        <option value="B70">B70</option>-->
<!--                    </select>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_left_item" id="steklopaketVal">-->
<!--                    <span class="name_calc_left_item">Стеклопакет</span>-->
<!--                    <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                    <ul class="notactive">-->
<!--                        <li data-value="Однокамерный">Однокамерный</li>-->
<!--                        <li data-value="Двухкамер. + энерго">Двухкамер. + энерго</li>-->
<!--                        <li data-value="Двухкамер. + мультифунк.">Двухкамер. + мультифунк.</li>-->
<!--                    </ul>-->
<!---->
<!--                    <select name="steklopaket" id="steklopaket" class="notactive">-->
<!--                        <option value="Однокамерный">Однокамерный</option>-->
<!--                        <option value="Двухкамер. + энерго">Двухкамер. + энерго</option>-->
<!--                        <option value="Двухкамер. + мультифунк.">Двухкамер. + мультифунк.</option>-->
<!--                    </select>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_left_item" id="furnituraVal">-->
<!--                    <span class="name_calc_left_item">Фурнитура</span>-->
<!--                    <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                    <ul class="notactive">-->
<!--                        <li data-value="Axor">Axor (Украина)</li>-->
<!--                        <li data-value="Siegenia">Siegenia (Германия)</li>-->
<!--                    </ul>-->
<!---->
<!--                    <select name="furnitura" id="furnitura" class="notactive">-->
<!--                        <option value="Axor">Axor (Украина)</option>-->
<!--                        <option value="Siegenia">Siegenia (Германия)</option>-->
<!--                    </select>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_left_item" id="podokonnikVal">-->
<!--                    <span class="name_calc_left_item">Подоконник</span>-->
<!--                    <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                    <ul class="notactive">-->
<!--                        <li data-value="1">1</li>-->
<!--                        <li data-value="2">2</li>-->
<!--                        <li data-value="3">3</li>-->
<!--                        <li data-value="4">4</li>-->
<!--                    </ul>-->
<!---->
<!--                    <select name="podokonnik" id="podokonnik" class="notactive">-->
<!--                        <option value="1">1</option>-->
<!--                        <option value="2">2</option>-->
<!--                        <option value="3">3</option>-->
<!--                        <option value="4">4</option>-->
<!--                    </select>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_left_item" id="otlivVal">-->
<!--                    <span class="name_calc_left_item">Отлив</span>-->
<!--                    <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                    <ul class="notactive">-->
<!--                        <li data-value="1">1</li>-->
<!--                        <li data-value="2">2</li>-->
<!--                        <li data-value="3">3</li>-->
<!--                        <li data-value="4">4</li>-->
<!--                    </ul>-->
<!---->
<!--                    <select name="otliv" id="otliv" class="notactive">-->
<!--                        <option value="1">1</option>-->
<!--                        <option value="2">2</option>-->
<!--                        <option value="3">3</option>-->
<!--                        <option value="4">4</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <p class="calc_steps_name">шаг 3: укажите размер и дополнительные комплектующие</p>-->
<!---->
<!---->
<!--            <div class="calc_size_characteristics">-->
<!--                <div id="width_slider_balcon_top" class="notactive">-->
<!--                    <div class="width_slider_input_wrap">-->
<!--                        <input type="text" id="contentSliderWidthTop"></input>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div id="height_slider">-->
<!--                    <div class="height_slider_input_wrap">-->
<!--                        <input type="text" id="contentSliderHeight"></input>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div id="width_slider">-->
<!--                    <div class="width_slider_input_wrap">-->
<!--                        <input type="text" id="contentSliderWidth"></input>-->
<!--                    </div>-->
<!--                    <span class="coordinate_start">0</span>-->
<!--                </div>-->
<!---->
<!--                <div id="height_slider_balcon_right" class="notactive">-->
<!--                    <div class="height_slider_input_wrap">-->
<!--                        <input type="text" id="contentSliderHeightRight"></input>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="win_in_calc" style="background-image: url('img/win_in_calc_1.png');"></div>-->
<!--            </div>-->
<!---->
<!--            <div class="calc_characteristics_right">-->
<!--                <div class="calc_characteristics_right_item">-->
<!--                    <p>Монтаж</p>-->
<!---->
<!--                    <div class="select_char">-->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="1" name="montaj" checked="checked">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">да</span>-->
<!--                        </label>-->
<!---->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="0" name="montaj">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">нет</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_right_item">-->
<!--                    <p>Москитная сетка</p>-->
<!---->
<!--                    <div class="select_char">-->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" name="moskit" value="1" checked="checked">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">да</span>-->
<!--                        </label>-->
<!---->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="0" name="moskit">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">нет</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_right_item">-->
<!--                    <p>Ламинация</p>-->
<!---->
<!--                    <div class="select_char">-->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="1" name="lamination" checked="checked">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">да</span>-->
<!--                        </label>-->
<!---->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="0" name="lamination">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">нет</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_right_item">-->
<!--                    <p>Откосы</p>-->
<!---->
<!--                    <div class="select_char">-->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="1" name="otkos" checked="checked">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">да</span>-->
<!--                        </label>-->
<!---->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="0" name="otkos">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">нет</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="calc_characteristics_right_item">-->
<!--                    <p>Вынос мусора</p>-->
<!---->
<!--                    <div class="select_char">-->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="1" name="musor" checked="checked">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">да</span>-->
<!--                        </label>-->
<!---->
<!--                        <label>-->
<!--                            <input class="radio" type="radio" value="0" name="musor">-->
<!--                            <span class="radio-custom"></span>-->
<!--                            <span class="label">нет</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!---->
<!--            <div class="calc_price">-->
<!--                Узнать цену-->
<!--            </div>-->
<!---->
<!--            <div class="calculated_price_block notactive">-->
<!--                <div class="calculated_full_price">-->
<!--                    <p class="calculated_price_block_name">Полная стоимость</p>-->
<!--                    <p class="calculated_price_block_value">2584 грн</p>-->
<!--                </div>-->
<!---->
<!--                <div class="calculated_sale">-->
<!--                    <p class="calculated_price_block_name">Скидка</p>-->
<!--                    <p class="calculated_price_block_value">10%</p>-->
<!--                </div>-->
<!---->
<!--                <div class="calculated_price_with_sale">-->
<!--                    <p class="calculated_price_with_sale_name">Итоговая стоимость</p>-->
<!--                    <p class="calculated_price_with_sale_value">2000 грн</p>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="calculator_recall_me notactive">-->
<!--                Перезвоните мне-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--</section>-->
<!---->
<!---->
<!--<script>-->
<!--    var calculator = {indix: {-->
<!--        url: '--><?//= Url::to(["/calculator/calculation-ajax"]) ?>//'
//    }
//    };
//</script>