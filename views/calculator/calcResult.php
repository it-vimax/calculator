<div class="calculated_price_block">
    <div class="calculated_full_price">
        <p class="calculated_price_block_name">Полная стоимость</p>
        <p class="calculated_price_block_value"><?= $_SESSION['totalPrice']['total_price'] ?> грн</p>
    </div>

    <div class="calculated_sale">
        <p class="calculated_price_block_name">Скидка</p>
        <p class="calculated_price_block_value"><?= $promotionArr["generalAdvertisingDiscount"] ?>%</p>
    </div>

    <div class="calculated_price_with_sale">
        <p class="calculated_price_with_sale_name">Итоговая стоимость</p>
        <p class="calculated_price_with_sale_value"><?= $salePrice ?> грн</p>
    </div>
</div>
<div class="calculator_recall_me">
    Перезвоните мне
</div>
<div class="telephone-number">
    <?php if(isset($phones)): ?>
        <?php foreach($phones as $phone): ?>
            <?= $phone->value ?><br>
        <?php endforeach; ?>
    <?php endif; ?>
</div>