<?php

use yii\helpers\Html;
use yii\helpers\Url;
session_start();
?>

<section class="calculator_page">
    <div class="calculator_wrapper">
        <div class="calc_head">
            <h1>Калькулятор окон</h1>
            <p class="calc_steps_name">шаг 1: Выберите конструкцию</p>
            <div class="calc_type_of_construction">
                <?= Html::img('@web/img/type_of_win1_active.png', ['id' => '1g']) ?>
                <?= Html::img('@web/img/type_of_win2.png', ['id' => '1p']) ?>
                <?= Html::img('@web/img/type_of_win3.png', ['id' => '2gp']) ?>
                <?= Html::img('@web/img/type_of_win4.png', ['id' => '3gpf']) ?>
                <?= Html::img('@web/img/type_of_win5.png', ['id' => '3gpg']) ?>
                <?= Html::img('@web/img/type_of_win6.png', ['id' => '3pgp']) ?>
                <?= Html::img('@web/img/type_of_win7.png', ['id' => '4gppg']) ?>
                <?= Html::img('@web/img/type_of_win8.png', ['id' => '1b1g', 'class' => 'balcon_constr']) ?>
                <?= Html::img('@web/img/type_of_win9.png', ['id' => '1b2gp', 'class' => 'balcon_constr']) ?>
            </div>

            <div class="need_other_construction">Нужна другая конструкция?</div>
        </div>

        <div class="calculator_body">
            <div class="message"></div>
            <div class="size-block">
            <p class="calc_steps_name">шаг 2: Выберите основные комплектующие</p>
            <div class="calc_characteristics_left">
                <div class="calc_characteristics_left_item" id="profileVal">
                    <span class="name_calc_left_item">Профиль</span>
                    <span class="small_item_name notactive">Профиль</span>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    <ul class="notactive profile-list">
                        <li data-value="lid">Lider</li>
                        <li data-value="b58">Viknaland B58</li>
                        <li data-value="b70">Viknaland B70</li>
                        <li data-value="Al4">Aluplast Ideal 4000</li>
                    </ul>

                    <select name="profile" id="profile" class="notactive">
                        <option value="lid">Lider</option>
                        <option value="b58">B58</option>
                        <option value="b70">B70</option>
                        <option value="Al4">Aluplast Ideal 4000</option>
                    </select>
                </div>

                <div class="calc_characteristics_left_item" id="steklopaketVal">
                    <span class="name_calc_left_item">Стеклопакет</span>
                    <span class="small_item_name notactive">Стеклопакет</span>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    <ul class="notactive steklopaket-list">
                        <li data-value="24-">Однокамерный</li>
                        <li data-value="32i">Двухкамер. + энерго</li>
                        <li data-value="32M">Двухкамер. + мультифунк.</li>
                    </ul>

                    <select name="steklopaket" id="steklopaket" class="notactive">
                        <option value="24-">Однокамерный</option>
                        <option value="32i">Двухкамер. + энерго</option>
                        <option value="32M">Двухкамер. + мультифунк.</option>
                    </select>
                </div>

                <div class="calc_characteristics_left_item" id="furnituraVal">
                    <span class="name_calc_left_item">Фурнитура</span>
                    <span class="small_item_name notactive">Фурнитура</span>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    <ul class="notactive furnitura-list">
                        <li data-value="ax">Axor (Украина)</li>
                        <li data-value="si">Siegenia (Германия)</li>
                    </ul>

                    <select name="furnitura" id="furnitura" class="notactive">
                        <option value="ax">Axor (Украина)</option>
                        <option value="si">Siegenia (Германия)</option>
                    </select>
                </div>

                <div class="calc_characteristics_left_item" id="podokonnikVal">
                    <span class="name_calc_left_item">Подоконник</span>
                    <span class="small_item_name notactive">Подоконник</span>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    <ul class="notactive">
                        <li data-value="">Нет</li>
                        <li data-value="100">100 мм</li>
                        <li data-value="200">200 мм</li>
                        <li data-value="300">300 мм</li>
                        <li data-value="400">400 мм</li>
                        <li data-value="500">500 мм</li>
                        <li data-value="600">600 мм</li>
                        <li data-value="700">700 мм</li>
                    </ul>

                    <select name="podokonnik" id="podokonnik" class="notactive">
                        <option value="">Нет</option>
                        <option value="100">100 мм</option>
                        <option value="200">200 мм</option>
                        <option value="300">300 мм</option>
                        <option value="400">400 мм</option>
                        <option value="500">500 мм</option>
                        <option value="600">600 мм</option>
                        <option value="700">700 мм</option>
                    </select>
                </div>

                <div class="calc_characteristics_left_item" id="otlivVal">
                    <span class="name_calc_left_item">Отлив</span>
                    <span class="small_item_name notactive">Отлив</span>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    <ul class="notactive">
                        <li data-value="">Нет</li>
                        <li data-value="50">50 мм</li>
                        <li data-value="70">70 мм</li>
                        <li data-value="100">100 мм</li>
                        <li data-value="120">120 мм</li>
                        <li data-value="150">150 мм</li>
                        <li data-value="200">200 мм</li>
                        <li data-value="250">250 мм</li>
                        <li data-value="300">300 мм</li>
                    </ul>

                    <select name="otliv" id="otliv" class="notactive">
                        <option value="">Нет</option>
                        <option value="70">70 мм</option>
                        <option value="100">100 мм</option>
                        <option value="120">120 мм</option>
                        <option value="150">150 мм</option>
                        <option value="200">200 мм</option>
                        <option value="250">250 мм</option>
                        <option value="300">300 мм</option>
                    </select>
                </div>
            </div>

            <p class="calc_steps_name">шаг 3: укажите размер и дополнительные комплектующие</p>


            <div class="calc_size_characteristics">
                <div id="width_slider_balcon_top" class="notactive">
                    <div class="width_slider_input_wrap">
                        <input type="text" id="contentSliderWidthTop" readonly="true"></input>
                    </div>
                </div>

                <div id="height_slider">
                    <div class="height_slider_input_wrap">
                        <input type="text" id="contentSliderHeight" readonly="true"></input>
                    </div>
                </div>

                <div id="width_slider">
                    <div class="width_slider_input_wrap">
                        <input type="text" id="contentSliderWidth" readonly="true"></input>
                    </div>
<!--                    <span class="coordinate_start">0</span>-->
                </div>

                <div id="height_slider_balcon_right" class="notactive">
                    <div class="height_slider_input_wrap">
                        <input type="text" id="contentSliderHeightRight" readonly="true"></input>
                    </div>
                </div>
                <div class="win_in_calc" style="background-image: url('img/win_in_calc_without_lam1.png');"></div>
                <span class="balcon_char_names win_w notactive">Ширина окна</span>
                <span class="balcon_char_names win_h notactive">Высота окна</span>
                <span class="balcon_char_names balc_w notactive">Ширина двери</span>
                <span class="balcon_char_names balc_h notactive">Высота двери</span>
            </div>

            <div class="calc_characteristics_right">
                <div class="calc_characteristics_right_item">
                    <p>Монтаж</p>

                    <div class="select_char">
                        <label>
                            <input class="radio" type="radio" value="1" name="montaj">
                            <span class="radio-custom"></span>
                            <span class="label">да</span>
                        </label>

                        <label>
                            <input class="radio" type="radio" value="0" name="montaj" checked="checked">
                            <span class="radio-custom"></span>
                            <span class="label">нет</span>
                        </label>
                    </div>
                </div>

                <div class="calc_characteristics_right_item notactive_item" id="mosquito_net">
                    <p>Москитная сетка</p>

                    <div class="select_char">
                        <label>
                            <input class="radio" type="radio" name="moskit" value="1">
                            <span class="radio-custom"></span>
                            <span class="label">да</span>
                        </label>

                        <label>
                            <input class="radio" type="radio" value="0" name="moskit" checked="checked">
                            <span class="radio-custom"></span>
                            <span class="label">нет</span>
                        </label>
                    </div>
                </div>

                <div class="calc_characteristics_right_item">
                    <p>Ламинация</p>

                    <div class="select_char">
                        <label>
                            <input class="radio" type="radio" value="1" name="lamination">
                            <span class="radio-custom"></span>
                            <span class="label">да</span>
                        </label>

                        <label>
                            <input class="radio" type="radio" value="0" name="lamination" checked="checked">
                            <span class="radio-custom"></span>
                            <span class="label">нет</span>
                        </label>
                    </div>
                </div>

                <div class="calc_characteristics_right_item">
                    <p>Откосы</p>

                    <div class="select_char">
                        <label>
                            <input class="radio" type="radio" value="1" name="otkos">
                            <span class="radio-custom"></span>
                            <span class="label">да</span>
                        </label>

                        <label>
                            <input class="radio" type="radio" value="0" name="otkos" checked="checked">
                            <span class="radio-custom"></span>
                            <span class="label">нет</span>
                        </label>
                    </div>
                </div>

                <div class="calc_characteristics_right_item">
                    <p>Вынос мусора</p>

                    <div class="select_char">
                        <label>
                            <input class="radio" type="radio" value="1" name="musor">
                            <span class="radio-custom"></span>
                            <span class="label">да</span>
                        </label>

                        <label>
                            <input class="radio" type="radio" value="0" name="musor" checked="checked">
                            <span class="radio-custom"></span>
                            <span class="label">нет</span>
                        </label>
                    </div>
                </div>
            </div></div>
            <div class="calc_price">
                Узнать цену
            </div>
            <div class="calculator_recall_me">Перезвоните мне</div>
        </div>
        <div class="calc-result-price"></div>
    </div>
</section>

<div class="calc_popup_form notactive">
    <form action="" onsubmit="return false;" id="calc_popup_form">
        <p>Перезвоните мне</p>
        <div class="name">
            <input type="text" name="name" placeholder="Имя">
            <div class="msh-no-valid msg-name"></div>
        </div>
        <div class="email">
            <input type="email" name="email" placeholder="Email">
        </div>
        <div class="tel">
            <input type="tel" name="phone" placeholder="+38(">
            <div class="msh-no-valid msg-phone"></div>
        </div>
        <div class="your-question-field">
            <textarea class="your-question" name="yourQuestion" placeholder="Ваш вопрос"></textarea>
        </div>

        <button class="btn-send">Отправить</button>
    </form>
    <div class="close_calc_popup_form">
        <span></span>
        <span></span>
    </div>
</div>

<div class="calc_other_consultation notactive">
    <form action="" onsubmit="return false;"  id="calc_other_consultation">
        <p>Нужна другая конструкция</p>
        <input type="hidden" name="other_consultation" value="true">
        <div class="name">
            <input type="text" name="name" placeholder="Имя">
            <div class="msh-no-valid msg-name"></div>
        </div>
        <div class="email">
            <input type="email" name="email" placeholder="Email">
        </div>
        <div class="tel">
            <input type="tel" name="phone" placeholder="+38(">
            <div class="msh-no-valid msg-phone"></div>
        </div>
        <div class="your-question-field">
            <textarea class="your-question" name="yourQuestion" placeholder="Ваш вопрос"></textarea>
        </div>
        <button class="btn-send">Отправить</button>
    </form>

    <div class="close_calc_popup_form">
        <span></span>
        <span></span>
    </div>
</div>
<script>
var calculator = {
    index: {
        url: '<?= Url::to(["/calculator/calculation-ajax"]) ?>'
    }
};
</script>