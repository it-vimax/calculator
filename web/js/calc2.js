var windowsSize = {
    w1g : {},
    w1p : {},
    w2gp : {},
    w3gpf : {},
    w3gpg : {},
    w3pgp : {},
    w4gppg : {},
    w1b : {
        width : 600,
        maxWidth : 900,
        height : 1900,
        maxHeight : 2200,
    },
};

var step = 50;
var height_input_val = windowsSize.w1g.height;
var width_input_val = windowsSize.w1g.width;
var height_input_val_max = windowsSize.w1g.maxHeight;
var width_input_val_max = windowsSize.w1g.maxWidth;
var balcon_height_input_val = windowsSize.w1b.height;
var balcon_width_input_val = windowsSize.w1b.width;
var balcon_height_input_val_max = windowsSize.w1b.maxHeight;
var balcon_width_input_val_max = windowsSize.w1b.maxWidth;
var characteristicsWindow = {};
var typeWindow = '1g';

var windowUrlImg = {};

/* загрузка размера окна */
function loadWindowSize(typeWindow)
{
    $.ajax({
        url: '/calculator/get-size-window',                 // указываем URL
        //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
        dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
        data : {windowType: typeWindow},	            //	даные, которые передаем
        async : true,	            //	асинхронность запроса, по умолчанию true
        cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
        contentType : "application/x-www-form-urlencoded",
        type : "get",            // GET либо POST

        success: function (data)
        {	// вешаем свой обработчик на функцию success
            if(!data)
            {
                console.warn('false');
            }
            for(var key in data)
            {
                windowsSize[key] = data[key];
            }
            sizeWindow();
        },

        error: function (error)
        {	// вешаем свой обработчик на функцию error
            console.log(error);
        },

        beforeSend: function(){},	//	срабатывает перед отправкой запроса
        complete: function(){}		//	срабатывает по окончанию запроса
    });
}
loadWindowSize(typeWindow);

function sizeWindow()
{
    if(typeWindow == '1b1g' || typeWindow == '1b2gp')
    {
        // console.log('выбрали двери ' + typeWindow);
        switch(typeWindow)
        {
            case '1b1g':
                width_input_val = windowsSize.w1g.width;
                height_input_val = windowsSize.w1g.height;
                height_input_val_max = windowsSize.w1g.maxHeight;
                width_input_val_max = windowsSize.w1g.maxWidth;
                break;

            case '1b2gp':
                width_input_val = windowsSize.w2gp.width;
                height_input_val = windowsSize.w2gp.height;
                height_input_val_max = windowsSize.w2gp.maxHeight;
                width_input_val_max = windowsSize.w2gp.maxWidth;
                break;
        }

        $('.balcon_char_names').removeClass('notactive');
    }
    else
    {	// устанавливаем новые параметры окна
        width_input_val = windowsSize['w' + typeWindow].width;
        height_input_val = windowsSize['w' + typeWindow].height;
        height_input_val_max = windowsSize['w' + typeWindow].maxHeight;
        width_input_val_max = windowsSize['w' + typeWindow].maxWidth;
        $('.balcon_char_names').addClass('notactive');
    }
    setWindowParams();
    $('#contentSliderHeight').val(height_input_val);
    $('#contentSliderWidth').val(width_input_val);
    balcon_height_input_val = windowsSize.w1b.height;
    balcon_width_input_val = windowsSize.w1b.width;
    $('#contentSliderHeightRight').val(windowsSize.w1b.height);
    $('#contentSliderWidthTop').val(windowsSize.w1b.width);
    balcon_height_input_val_max = windowsSize.w1b.maxHeight;
    balcon_width_input_val_max = windowsSize.w1b.maxWidth;
    $("#width_slider_balcon_top").slider({
        orientation: "horizontal",
        value : balcon_width_input_val,//Значение, которое будет выставлено слайдеру при загрузке
        min : balcon_width_input_val,//Минимально возможное значение на ползунке
        max : balcon_width_input_val_max,//Максимально возможное значение на ползунке
        step : step,//Шаг, с которым будет двигаться ползунок
        create: function( event, ui ) {
            val = $( "#width_slider_balcon_top" ).slider("value");//При создании слайдера, получаем его значение в перемен. val
            $( "#contentSliderWidthTop" ).val( val );//Заполняем этим значением элемент с id contentSlider
        },
        slide: function( event, ui ) {
            $( "#contentSliderWidthTop" ).val( ui.value );//При изменении значения ползунка заполняем элемент с id contentSlider
            showBtnWhatPrice();
        }
    });
    $("#height_slider_balcon_right").slider({
        orientation: "vertical",
        value : balcon_height_input_val,//Значение, которое будет выставлено слайдеру при загрузке
        min : balcon_height_input_val,//Минимально возможное значение на ползунке
        max : balcon_height_input_val_max,//Максимально возможное значение на ползунке
        step : step,//Шаг, с которым будет двигаться ползунок
        create: function( event, ui ) {
            val = $( "#height_slider_balcon_right" ).slider("value");//При создании слайдера, получаем его значение в перемен. val
            $( "#contentSliderHeightRight" ).val( balcon_height_input_val );//Заполняем этим значением элемент с id contentSlider
        },
        slide: function( event, ui ) {
            $( "#contentSliderHeightRight" ).val( ui.value );//При изменении значения ползунка заполняем элемент с id contentSlider
            showBtnWhatPrice();
        }
    });
}

function showBtnWhatPrice()
{
    $('.calculated_price_block').remove();
    $('.calculator_recall_me').remove();
    $('.telephone-number').remove();
    $('.calc_price').remove();
    $('.calculator_body').append("<div class='calc_price'>Узнать цену</div>");
    $('.calculator_body').append("<div class='calculator_recall_me'>Перезвоните мне</div><div class='telephone-number'></div>");
}

$('#furnituraVal').addClass('notactive_item'); // изначально прячем фурнитуру
$('.calc_type_of_construction img').on('click', function()
{
    showBtnWhatPrice();

    var windowId = $(this).attr('id');
    loadWindowSize(windowId);
    typeWindow = $(this).attr('id');	//	записываем текущее активное окно

    switch(typeWindow)
    {
        case "1b1g":
            $('#mosquito_net').addClass('notactive_item');
            if($('#furnituraVal').hasClass('notactive_item'))
            {
                $('#furnituraVal').removeClass('notactive_item');
            }
            break;
        case '1g':
            $('#furnituraVal').addClass('notactive_item');
            $('#mosquito_net').addClass('notactive_item');
            break;
        default:
            $('#furnituraVal').removeClass('notactive_item');
            $('#mosquito_net').removeClass('notactive_item');
    }

    var this_index = $(this).index() + 1;

    $('.calc_type_of_construction img').each(function(){
        var index = $(this).index() + 1;
        $(this).attr('src',"img/type_of_win" + index + ".png");
    });

    $(this).attr('src',"img/type_of_win" + this_index + "_active.png");

    if($(this).hasClass('balcon_constr')) {
        $('#width_slider_balcon_top').removeClass('notactive');
        $('#height_slider_balcon_right').removeClass('notactive');
    }
    else {
        $('#width_slider_balcon_top').addClass('notactive');
        $('#height_slider_balcon_right').addClass('notactive');
    }
    lamination();
});


function setWindowParams()
{
    $("#width_slider").slider({
        value : width_input_val,//Значение, которое будет выставлено слайдеру при загрузке
        min : width_input_val,//Минимально возможное значение на ползунке
        max : width_input_val_max,//Максимально возможное значение на ползунке
        step : step,//Шаг, с которым будет двигаться ползунок
        create: function( event, ui ) {
            val = $( "#width_slider" ).slider("value");//При создании слайдера, получаем его значение в перемен. val
            $( "#contentSliderWidth" ).val( val );//Заполняем этим значением элемент с id contentSlider
        },
        slide: function( event, ui ) {
            $( "#contentSliderWidth" ).val( ui.value );//При изменении значения ползунка заполняем элемент с id contentSlider
            showBtnWhatPrice();
        }
    });

    $("#height_slider").slider({
        orientation: "vertical",
        value : height_input_val,//Значение, которое будет выставлено слайдеру при загрузке
        min : height_input_val,//Минимально возможное значение на ползунке
        max : height_input_val_max,//Максимально возможное значение на ползунке
        step : step,//Шаг, с которым будет двигаться ползунок
        create: function( event, ui ) {
            val = $( "#height_slider" ).slider("value");//При создании слайдера, получаем его значение в перемен. val
            $( "#contentSliderHeight" ).val( val );//Заполняем этим значением элемент с id contentSlider
        },
        slide: function( event, ui ) {
            $( "#contentSliderHeight" ).val( ui.value );//При изменении значения ползунка заполняем элемент с id contentSlider
            showBtnWhatPrice();
        }
    });

    // Изменение значения на ввод пользователя высоты
    $('#contentSliderHeight').on('change',function(){
        showBtnWhatPrice();
        height_input_val = $(this).val();
        // console.log(contentSliderHeight);
        var valPercent_bottom = height_input_val/height_input_val_max * 100;
        // console.log(valPercent_bottom);

        if (valPercent_bottom <= 100) {
            valPercent_bottom = valPercent_bottom + "%";
            $('#height_slider .ui-slider-handle').css('bottom', valPercent_bottom);
        }
        else {
            alert('Максимальное знаечие 5000 мм!');
        }
    });

    // Изменение значения на ввод пользователя длины
    $('#contentSliderWidth').on('change',function(){
        width_input_val = $(this).val();
        // console.log(width_input_val);
        var valPercent_left = width_input_val/height_input_val_max * 100;
        // console.log(valPercent_left);
        if (valPercent_left <= 100) {
            valPercent_left = valPercent_left + "%";
            $('#width_slider .ui-slider-handle').css('left', valPercent_left);
        }
        else {
            alert('Максимальное знаечие 5000 мм!');
        }
    });

    // Изменение значения на ввод пользователя высоты
    $('#contentSliderHeightRight').on('change',function(){
        height_input_val = $(this).val();
        // console.log(contentSliderHeight);
        var valPercent_bottom = height_input_val/height_input_val_max * 100;
        // console.log(valPercent_bottom);

        if (valPercent_bottom <= 100) {
            valPercent_bottom = valPercent_bottom + "%";
            $('#height_slider_balcon_right .ui-slider-handle').css('bottom', valPercent_bottom);
        }
        else {
            alert('Максимальное знаечие 5000 мм!');
        }
    });

    // Изменение значения на ввод пользователя длины
    $('#contentSliderWidthTop').on('change',function(){
        width_input_val = $(this).val();
        // console.log(width_input_val);
        var valPercent_left = width_input_val/height_input_val_max * 100;
        // console.log(valPercent_left);
        if (valPercent_left <= 100) {
            valPercent_left = valPercent_left + "%";
            $('#width_slider_balcon_top .ui-slider-handle').css('left', valPercent_left);
        }
        else {
            alert('Максимальное знаечие 5000 мм!');
        }
    });

}

$(document).ready(function()
{
    setWindowParams();


});
/* --------------------------------------------------------------- Click ajax on right ------------------------- */

$('.radio').change(function()
{
    showBtnWhatPrice();
});

$('.calc_characteristics_left').on('click', function()
{
    showBtnWhatPrice();
});

 $(document).mouseup(function (e) {
     var container = $(".calc_characteristics_left_item");

     if (container.has(e.target).length === 0){


         if($(e.target).hasClass('calc_characteristics_left_item')){

             if ($(e.target).hasClass('active_left_item')) {

                 $(e.target).removeClass('active_left_item');

             } else {
                 $(e.target).addClass('active_left_item');
             }

             $(e.target).find('ul').toggleClass('notactive');


         } else {
             container.removeClass('active_left_item');
             container.children('ul').addClass('notactive');
         }

    } else {

         if (container.hasClass('active_left_item')) {
             container.removeClass('active_left_item');

         } else {
             container.addClass('active_left_item');
         }

         $(e.target).parents('.calc_characteristics_left_item').find('ul').toggleClass('notactive');
     }

 });

$('.calc_characteristics_left_item').on('click', function()
{
    var loc = this;

    $('.calc_characteristics_left_item').each(function(index, value)
    {
        if($(loc).attr('id') != $(value).attr('id'))
        {
            $(value).find('ul').addClass('notactive');
        }
    });
});

$('.calc_characteristics_left_item ul li').on('click',function(){
    var list_text = $(this).text();
    var dataValue = $(this).attr('data-value');
    characteristicsWindow[$(this).parent().parent().attr('id')] = dataValue;
    $(this).parent('ul').siblings('.name_calc_left_item').text(list_text);

    $(this).parent('ul').siblings('.small_item_name').removeClass('notactive');
    // $(this).parent('ul').siblings('.small_item_name').text(item_name);
});

$('body').on('click', '.calculator_recall_me',function(){
    $('.popup').removeClass('notactive');
    $('.calc_popup_form').removeClass('notactive');
    $('body').addClass('body_popup_active');
});

$('.need_other_construction').on('click', function() {
    $('.calc_other_consultation').removeClass('notactive');
});

$('.calc_other_consultation .close_calc_popup_form').on('click', function() {
    $('.calc_other_consultation').addClass('notactive');
});

$('body').on('click', '.calculator_other_consultation',function(){
    console.log('click calculator_other_consultation');
    $('.popup').removeClass('notactive');
    $('.calc_other_consultation').removeClass('notactive');
    $('body').addClass('body_popup_active');
});

$('.popup').on('click',function(){
    $('.popup').addClass('notactive');
    $('.calc_popup_form').addClass('notactive');
    $('body').removeClass('body_popup_active');
});

$('.close_calc_popup_form').on('click', function() {
    $('.popup').addClass('notactive');
    $('.calc_popup_form').addClass('notactive');
    $('body').removeClass('body_popup_active');
});

$('[name="lamination"]').change(function()
{
    lamination();
});

function lamination()
{
    var param = $('[name="lamination"]:checked').val();
    var indx = $('#' + typeWindow).index() + 1;
    if(+param !== 1)
    {
        $('.win_in_calc').css("backgroundImage", "url('img/win_in_calc_without_lam" + indx + ".png')");
    }
    else
    {
        $('.win_in_calc').css("backgroundImage", "url('img/win_in_calc_" + indx + ".png')");
    }
}

/* опускаем список */
//--
$('#profileVal li[data-value]').on('click', function()
{
    console.log($(this).data("value"));
    if($(this).data("value") == "Al4")
    {
        $('.calc_characteristics_left_item ul.profile-list').addClass('below-steklopaket');
    }
    else
    {
        $('.calc_characteristics_left_item ul.profile-list').removeClass('below-steklopaket');
    }
});
//--
$('#steklopaketVal li[data-value]').on('click', function()
{
    console.log($(this).data("value"));
    if($(this).data("value") == "24-")
    {
        $('.calc_characteristics_left_item ul.steklopaket-list').removeClass('below-steklopaket');
    }
    else
    {
        $('.calc_characteristics_left_item ul.steklopaket-list').addClass('below-steklopaket');
    }
});
//---
$('#furnituraVal li[data-value="si"]').on('click', function()
{
    console.log($(this).data("value"));
    if($(this).data("value") == "si")
    {
        $('.calc_characteristics_left_item ul.furnitura-list').addClass('below-furnitura');
    }
    else
    {
        $('.calc_characteristics_left_item ul.furnitura-list').removeClass('below-furnitura');
    }

});
/* end опускаем список */
/* Отправка формы */
$('input[name="phone"').mask('+38 (000) 000-00-00');
$("body").on('click', ".btn-send", function(e)
{
    var form = $(this).parent();
    var formId = $(form).attr('id');
    var name = $("#"+formId+" input[name='name']").val() || '';
    var phone = $("#"+formId+" input[name='phone']").val() || '';
    var email = $("#"+formId+" input[name='email']").val() || '';
    var ask = $("#"+formId+" textarea").val() || '';
    if(name.length <= 0)
    {
        $("#"+formId+" .msg-name").text('Поле имени обьязательно для заполнения!');
        return false;
    }
    else
    {
        $("#" + formId + " .msg-name").text('');
    }
    if(phone.length !== 19)
    {
        $("#"+formId+" .msg-phone").text('Поле номера телефона должно иметь 12 цыфр !');
        return false;
    }
    else
    {
        $("#"+formId+" .msg-phone").text('');
    }
    if(formId == "calc_other_consultation")
    {
        other_consultation = 'yes'
    }
    else
    {
        other_consultation = '';
    }
    console.log("formId =" + formId);
    console.log("other_consultation =" + other_consultation);
    $("#"+formId+" input[name='name']").val('');
    $("#"+formId+" input[name='phone']").val('');
    $("#"+formId+" input[name='email']").val('');
    $("#"+formId+" textarea").val('');
    $.ajax({
        url: "/calculator/call-me",                 // указываем URL
    	//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
        dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
    	data : {
            name : name,
            phone : phone,
            email : email,
            ask : ask,
            other_consultation : other_consultation,
        },	            //	даные, которые передаем
    	async : true,	            //	асинхронность запроса, по умолчанию true
    	cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
    	contentType : "application/x-www-form-urlencoded",
    	type : "post",            // GET либо POST
    	
        success: function (data)
    	{	// вешаем свой обработчик на функцию success
    	    if(!data)
    	    {
    	        console.warn('false');
    	        return;
    	    }
            $('.calc_other_consultation').addClass('notactive');
            $('.calc_popup_form').addClass('notactive');
            $('body').removeClass('body_popup_active');
    	    if(data.success)
            {
                $(".message").html("<h2>"+data.success+"</h2>");
            }
            if(data.error)
            {
                $(".message").html("<h2 style='color:red'>"+data.error+"</h2>");
            }
            setTimeout(function()
            {
                $('.message').html('');
            }, 5000);
        },
    	
    	error: function (error)
    	{	// вешаем свой обработчик на функцию error
    		console.log(error);
    	},
    	
    	beforeSend: function(){},	//	срабатывает перед отправкой запроса
    	complete: function(){}		//	срабатывает по окончанию запроса
    });
});
/* end Отправка формы */
