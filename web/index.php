<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

require __DIR__ . '/../functions.php';  //  вспомагалельные функции
require __DIR__ . '/../phpexcel/PHPExcel.php';  //  библиотека для работы с exel
require __DIR__ . '/../libs/Calculator.php';  //  Клас подсчета калькулятора

(new yii\web\Application($config))->run();
