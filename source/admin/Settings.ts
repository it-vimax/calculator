import _Send = require('./Send'); import Send = _Send.Send;
import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;

export class Settings extends Send implements IAdminClickEvent
{
//    public constructor()
//    {}

    public onEvent():void
    {
        let local = this;
        $("body").on("click", "#btn-dollarExchangeRate-change", function(event)
        {
            event.preventDefault();
            local.dollarExchangeRate(event, $(this));
        });
        $("body").on("click", "#btn-cache-change", function(event)
        {
            event.preventDefault();
            local.cacheUpdate(event, $(this))
        });
    }

    public cacheUpdate(event:any, element:any):void
    {
        console.log('enter cache');
        this.url = '/admin/setting/cache-update';
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }

    public dollarExchangeRate(event:any, element:any):void
    {
        this.url = '/admin/setting/dollar-exchange-rate-change';
        this.data['dollarExchangeRate'] = $('#dollarExchangeRate').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }

    public onSuccess(data:any):void
    {
        this.showMessage(data.message, data.nameField);
    }
}