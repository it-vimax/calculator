import _Send = require('./Send'); import Send = _Send.Send;
import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;

export class Phone extends Send implements IAdminClickEvent
{
    public onEvent():void
    {
        let local = this;
        $("body").on("click", "#phone-add", function(event)  //  создание нового email
        {
            event.preventDefault();
            local.add(event, $(this));
        });
        $("body").on("click", "#btn-phone-delete", function(event)
        {
            event.preventDefault();
            local.delete(event, $(this));
        });
    }

    public delete(event:any, element:any):void
    {
        this.url = '/admin/default/phone-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');

    }

    public onDelete(data:any):void
    {
        $('#phone-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }

    public add(event:any, element:any):void
    {
        this.url = '/admin/default/phone-add';
        this.data['phone'] = $('[name="phone"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    }

    public onAdd(data:any):void
    {
        $('[name="phone"]').val('');
        $('#phone-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }
}