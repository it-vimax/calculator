import _Send = require('./Send'); import Send = _Send.Send;
import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;

export class Outflows extends Send implements IAdminClickEvent
{
//    public constructor(){}
    
    public onEvent():void
    {
        let local = this;
        $("body").on("click", "#btn-outflows-add", function(event)
        {
            event.preventDefault();
            local.add(event, $(this));
        });

        $("body").on("click", "#btn-outflows-delete", function(event)
        {
            event.preventDefault();
            local.delete(event, $(this));
        });
    }

    public delete(event:any, element:any):void
    {
        this.url = '/admin/window-items/outflows-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');
    }

    public onDelete(data:any):void
    {
        $('#outflow-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }

    public add(event:any, element:any):void
    {
        console.log($('[name="outflows-new-depth"]').val());
        this.url = '/admin/window-items/outflows-add';
        this.data['outflowDepth'] = $('[name="outflows-new-depth"]').val();
        this.data['outflowPrice'] = $('[name="outflows-new-price"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    }
    
    public onAdd(data:any):void
    {
        $('[name="outflows-new-depth"]').val('');
        $('[name="outflows-new-price"]').val('');
        $('#outflow-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }
}