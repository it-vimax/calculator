import _Send = require('./Send'); import Send = _Send.Send;
import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;

export class Promotions extends Send implements IAdminClickEvent
{
//    public constructor()
//    {}

    public onEvent():void
    {
        let local = this;
        $("body").on("click", "#btn-generalAdvertisingDiscount-change", function(event)
        {
            event.preventDefault();
            local.advertisingDiscount(event, $(this));
        });

        $("body").on("click", "#btn-discountOnWindows-change", function(event)
        {
            event.preventDefault();
            local.discountOnWindows(event, $(this));
        });
    }

    public discountOnWindows(event:any, element:any):void
    {
        this.url = '/admin/promotions/discount-on-windows-change';
        this.data['discountOnWindows'] = $('#discountOnWindows').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }

    public advertisingDiscount(event:any, element:any):void
    {
        this.url = '/admin/promotions/general-advertising-discount-change';
        this.data['generalAdvertisingDiscount'] = $('#generalAdvertisingDiscount').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }

    public onSuccess(data:any):void
    {
        this.showMessage(data.message, data.nameField);
    }
}