import _Send = require('./Send'); import Send = _Send.Send;
import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;

export class AdditionalWorks extends Send implements IAdminClickEvent
{
//    public constructor()
//    {}

    public onEvent():void
    {
        let local = this;
        $("body").on("click", "#btn-slopes", function(event)
        {
            event.preventDefault();
            local.slopes(event, $(this));
        });

        $("body").on("click", "#btn-windowInstalation", function(event)
        {
            event.preventDefault();
            local.windowInstalation(event, $(this));
        });

        $("body").on("click", "#btn-mosquitoNet", function(event)
        {
            event.preventDefault();
            local.mosquitoNet(event, $(this));
        });

        $("body").on("click", "#btn-gorbageRemoval", function(event)
        {
            event.preventDefault();
            local.gorbageRemoval(event, $(this));
        });
    }

    public gorbageRemoval(event:any, element:any):void
    {
        this.url = '/admin/additional-works/gorbage-removal-change';
        this.data['gorbageRemoval'] = $('#gorbageRemoval').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }

    public mosquitoNet(event:any, element:any):void
    {
        this.url = '/admin/additional-works/mosquito-net-change';
        this.data['mosquitoNet'] = $('#mosquitoNet').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }

    public windowInstalation(event:any, element:any):void
    {
        this.url = '/admin/additional-works/window-instalation-change';
        this.data['windowInstalation'] = $('#windowInstalation').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }
    
    public slopes(event:any, element:any):void
    {
        this.url = '/admin/additional-works/slopes-change';
        this.data['slopes'] = $('#slopes').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    }

    public onSuccess(data:any):void
    {
        this.showMessage(data.message, data.nameField);
    }
}