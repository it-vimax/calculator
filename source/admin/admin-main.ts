import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;
import _Email = require('./Email'); import Email = _Email.Email;
import _Phone = require('./Phone'); import Phone = _Phone.Phone;
import _AdditionalWorks = require('./AdditionalWorks'); import AdditionalWorks = _AdditionalWorks.AdditionalWorks;
import _Sills = require('./Sills'); import Sills = _Sills.Sills;
import _Outflows = require('./Outflows'); import Outflows = _Outflows.Outflows;
import _LoadFile = require('./LoadFile'); import LoadFile = _LoadFile.LoadFile;
import _Promotions = require('./Promotions'); import Promotions = _Promotions.Promotions;
import _Settings = require('./Settings'); import Settings = _Settings.Settings;

class Main
{
    public static main():void
    {
        // Обратная связь
        let email = new Email();
        email.onEvent();
        let phone = new Phone();
        phone.onEvent();
        // Компоненты окна
        let sills = new Sills();
        sills.onEvent();
        let outflows = new Outflows();
        outflows.onEvent();
        // дополнительные работы
        let additionalWorks = new AdditionalWorks();
        additionalWorks.onEvent();
        //  загрузка файла
        let loadFile = new LoadFile();
        loadFile.onEvent();
        // акции
        let promotions = new Promotions();
        promotions.onEvent();
        // настройки
        let settings = new Settings();
        settings.onEvent();
    }
}

$(document).ready(function(event)
{
    Main.main();
});