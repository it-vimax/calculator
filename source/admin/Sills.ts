import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;
import _Send = require('./Send'); import Send = _Send.Send;

export class Sills extends Send implements IAdminClickEvent
{
//    public constructor(){}
    
    public onEvent():void
    {
        let local = this;
        $("body").on("click", "#btn-sills-add", function(event)
        {
            event.preventDefault();
            local.add(event, $(this));
        });

        $("body").on("click", "#btn-sill-delete", function(event)
        {
            event.preventDefault();
            local.delete(event, $(this));
        });
    }

    public delete(event:any, element:any):void
    {
        this.url = '/admin/window-items/sill-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');
    }

    public onDelete(data:any):void
    {
        $('#sill-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }

    public add(event:any, element:any):void
    {
        this.url = '/admin/window-items/sill-add';
        this.data['sillDepth'] = $('[name="sill-new-depth"]').val();
        this.data['sillPrice'] = $('[name="sill-new-price"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    }

    public onAdd(data:any):void
    {
        $('[name="sill-new-depth"]').val('');
        $('[name="sill-new-price"]').val('');
        $('#sill-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }
}