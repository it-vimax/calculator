import _Email = require('./Email'); import Email = _Email.Email;

export class Send
{
    public url:string;
    public data:any = {};
    public type:string = "get";
    public processData:boolean = true;
    public dataType:any = 'text';
    public contentType:any = "application/x-www-form-urlencoded";

    public send(callback:string):void
    {
        this.data['param'] = $('meta[name=csrf-param]').attr("content");
        this.data['token'] = $('meta[name=csrf-token]').attr("content");
        let local = this;
        $.ajax({
            url: local.url,                 // указываем URL
            //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            dataType : this.dataType,	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
            data : local.data,	            //	даные, которые передаем
            async : true,	            //	асинхронность запроса, по умолчанию true
            cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
            contentType : this.contentType,
            processData : this.processData, // false если передаем файл
            type : local.type,            // GET либо POST

            success: function (data)
            {	// вешаем свой обработчик на функцию success
                if(data.messageType !== 'success')
                {
                    local.error(data);
                    return;
                }
                let callbackMethod = 'local.'+ callback +'(data)';
                eval(callbackMethod);
            },

            error: function (error)
            {	// вешаем свой обработчик на функцию error
                console.log(error);
            },

            beforeSend: function(){},	//	срабатывает перед отправкой запроса
            complete: function(){}		//	срабатывает по окончанию запроса
        });
    }
    
    public error(data:any):void
    {
        this.processData = true;
        this.contentType = "application/x-www-form-urlencoded";
        this.data = {};
        this.showMessage(data.message, data.nameField);
    }

    public showMessage(message:string, attribute:string, showTime:number = 2000):void
    {
        $(attribute).html(message);
        $(attribute).fadeIn(10);
        setTimeout(function()
        {
            $(attribute).fadeOut(1000);
        }, showTime);
    }
}