import _Send = require('./Send'); import Send = _Send.Send;
import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;

export class LoadFile extends Send implements IAdminClickEvent
{
    public fileAjax:any;
//    public constructor(){}

    public onEvent():void
    {
        let local = this;
        $('[name="file-excel-load"]').change(function (event)
        {
            local.fileAjax = this.files;
            local.change(event, $(this));
        });

        $("body").on("click", "#btn-load-file", function(event)
        {
            event.preventDefault();
            local.fileAjax = $('[name="file-excel-load"]').prop("files")[0];
            console.log(local.fileAjax);
            local.change(event, $('[name="file-excel-load"]'));
        });

        $("body").on("click", '[btn-id="btn-excel-delete"]', function(event)
        {
            event.preventDefault();
            local.deleteExcelFile(event, $(this));
        });
    }

    public deleteExcelFile(event:any, element:any):void
    {
        this.url = '/admin/load-files/delete-excel';
        this.type = 'post';
        this.dataType = 'json';
        this.data['delete'] = $(element).attr('data-id');
        this.send('onDeleteExcelFile');
    }

    public onDeleteExcelFile(data:any):void
    {
        $("#table-excel tbody").html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }


    public change(event:any, element:any):void
    {
        this.url = '/admin/load-files/save-excel';
        this.type = 'post';
        this.dataType = 'json';
        let data = new FormData();
        $.each(this.fileAjax, function (key, value)
        {
            data.append(key, value);
        });
        this.data = data;
        this.processData = false; // Не обрабатываем файлы (Don't process the files)
        this.contentType = false; // Так jQuery скажет серверу что это строковой запрос this.processData = false;
        this.send('onLoad');
    }

    public onLoad(data:any):void
    {
        this.processData = true;
        this.contentType = "application/x-www-form-urlencoded";
        this.data = {};
        $("#table-excel tbody").html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }
}
