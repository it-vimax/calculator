import _Send = require('./Send'); import Send = _Send.Send;
import _IAdminClickEvent = require('./interface/IAdminClickEvent'); import IAdminClickEvent = _IAdminClickEvent.IAdminClickEvent;

export class Email extends Send implements IAdminClickEvent
{
    public onEvent():void
    {
        let local = this;
        $("body").on("click", "#email-add", function(event)  //  создание нового email
        {
            event.preventDefault();
            local.add(event, $(this));
        });
        $("body").on("click", "#btn-email-delete", function(event)
        {
            event.preventDefault();
            local.delete(event, $(this));
        });
    }

    public delete(event:any, element:any):void
    {
        this.url = '/admin/default/email-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');

    }

    public onDelete(data:any):void
    {
        $('#email-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }

    public add(event:any, element:any):void
    {
        this.url = '/admin/default/email-add';
        this.data['email'] = $('[name="email"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    }

    public onAdd(data:any):void
    {
        $('[name="email"]').val('');
        $('#email-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    }
}