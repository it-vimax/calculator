export class Calculator
{
    static sendData: any; // обьект для отправки ajax
    static url: string;
    //  url отправки

    static onSend()
    {
        // формирование обьекта запроса
        let sendData:any = {
            montaj : $('[name="montaj"]:checked').val() || 0,
            moskit :($('[name="moskit"]:checked').val()) || 0,
            lamination : $('[name="lamination"]:checked').val() || 0,
            otkos : $('[name="otkos"]:checked').val() || 0,
            musor : $('[name="musor"]:checked').val() || 0,
            contentSliderHeight : $('#contentSliderHeight').val() || 0,
            contentSliderWidth : $('#contentSliderWidth').val() || 0,
            contentSliderWidthTop : $('#contentSliderWidthTop').val() || 0,
            contentSliderHeightRight : $('#contentSliderHeightRight').val() || 0,
            characteristicsWindow : characteristicsWindow || null,
            typeWindow : typeWindow || null
        };

        Calculator.sendAjax(sendData);
    }

    clickCalcPrice()
    {
        $('body').on('click', '.calc_price', Calculator.onSend);
    }

    static sendAjax(sendData:any):void
    {
        $.ajax({
            contentType: '/application/json',
            dataType: 'json',
            type: 'GET',
            url: calculator.index.url,
            data: {data: sendData},
            success: function (data) {
                $('body').on('click', '.calc_price', Calculator.onSend);
                if(data.warning)
                {
                    return $('.calc-result-price').html('<h5 style="color:red">*'+ data.warning +'</h5>');
                }
                if(data.error)
                {
                    return $('.calc-result-price').html('<h5>Файл excel не найден</h5>');
                }
                $('.calc-result-price').empty();
                $('.calc_price').remove();
                $('.calculator_recall_me').remove();
                $('.telephone-number').remove();
                $('.calculator_body').append(data);
            },
            error: function (error) {
                console.log(error);
            }
        });
        $("body").off('click', ".calc_price", Calculator.onSend);
    }
}