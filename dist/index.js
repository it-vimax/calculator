"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Calculator = require("./Calculator");
var Calculator = _Calculator.Calculator;
var Main = (function () {
    function Main() {
    }
    Main.main = function () {
        var calculator = new Calculator();
        calculator.clickCalcPrice();
    };
    return Main;
}());
$(document).ready(function () {
    Main.main();
});
//# sourceMappingURL=index.js.map