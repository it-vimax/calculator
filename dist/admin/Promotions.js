"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var Promotions = (function (_super) {
    __extends(Promotions, _super);
    function Promotions() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    //    public constructor()
    //    {}
    Promotions.prototype.onEvent = function () {
        var local = this;
        $("body").on("click", "#btn-generalAdvertisingDiscount-change", function (event) {
            event.preventDefault();
            local.advertisingDiscount(event, $(this));
        });
        $("body").on("click", "#btn-discountOnWindows-change", function (event) {
            event.preventDefault();
            local.discountOnWindows(event, $(this));
        });
    };
    Promotions.prototype.discountOnWindows = function (event, element) {
        this.url = '/admin/promotions/discount-on-windows-change';
        this.data['discountOnWindows'] = $('#discountOnWindows').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    Promotions.prototype.advertisingDiscount = function (event, element) {
        this.url = '/admin/promotions/general-advertising-discount-change';
        this.data['generalAdvertisingDiscount'] = $('#generalAdvertisingDiscount').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    Promotions.prototype.onSuccess = function (data) {
        this.showMessage(data.message, data.nameField);
    };
    return Promotions;
}(Send));
exports.Promotions = Promotions;
//# sourceMappingURL=Promotions.js.map