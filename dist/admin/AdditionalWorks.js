"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var AdditionalWorks = (function (_super) {
    __extends(AdditionalWorks, _super);
    function AdditionalWorks() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    //    public constructor()
    //    {}
    AdditionalWorks.prototype.onEvent = function () {
        var local = this;
        $("body").on("click", "#btn-slopes", function (event) {
            event.preventDefault();
            local.slopes(event, $(this));
        });
        $("body").on("click", "#btn-windowInstalation", function (event) {
            event.preventDefault();
            local.windowInstalation(event, $(this));
        });
        $("body").on("click", "#btn-mosquitoNet", function (event) {
            event.preventDefault();
            local.mosquitoNet(event, $(this));
        });
        $("body").on("click", "#btn-gorbageRemoval", function (event) {
            event.preventDefault();
            local.gorbageRemoval(event, $(this));
        });
    };
    AdditionalWorks.prototype.gorbageRemoval = function (event, element) {
        this.url = '/admin/additional-works/gorbage-removal-change';
        this.data['gorbageRemoval'] = $('#gorbageRemoval').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    AdditionalWorks.prototype.mosquitoNet = function (event, element) {
        this.url = '/admin/additional-works/mosquito-net-change';
        this.data['mosquitoNet'] = $('#mosquitoNet').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    AdditionalWorks.prototype.windowInstalation = function (event, element) {
        this.url = '/admin/additional-works/window-instalation-change';
        this.data['windowInstalation'] = $('#windowInstalation').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    AdditionalWorks.prototype.slopes = function (event, element) {
        this.url = '/admin/additional-works/slopes-change';
        this.data['slopes'] = $('#slopes').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    AdditionalWorks.prototype.onSuccess = function (data) {
        this.showMessage(data.message, data.nameField);
    };
    return AdditionalWorks;
}(Send));
exports.AdditionalWorks = AdditionalWorks;
//# sourceMappingURL=AdditionalWorks.js.map