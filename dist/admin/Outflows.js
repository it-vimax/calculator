"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var Outflows = (function (_super) {
    __extends(Outflows, _super);
    function Outflows() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    //    public constructor(){}
    Outflows.prototype.onEvent = function () {
        var local = this;
        $("body").on("click", "#btn-outflows-add", function (event) {
            event.preventDefault();
            local.add(event, $(this));
        });
        $("body").on("click", "#btn-outflows-delete", function (event) {
            event.preventDefault();
            local.delete(event, $(this));
        });
    };
    Outflows.prototype.delete = function (event, element) {
        this.url = '/admin/window-items/outflows-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');
    };
    Outflows.prototype.onDelete = function (data) {
        $('#outflow-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    Outflows.prototype.add = function (event, element) {
        console.log($('[name="outflows-new-depth"]').val());
        this.url = '/admin/window-items/outflows-add';
        this.data['outflowDepth'] = $('[name="outflows-new-depth"]').val();
        this.data['outflowPrice'] = $('[name="outflows-new-price"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    };
    Outflows.prototype.onAdd = function (data) {
        $('[name="outflows-new-depth"]').val('');
        $('[name="outflows-new-price"]').val('');
        $('#outflow-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    return Outflows;
}(Send));
exports.Outflows = Outflows;
//# sourceMappingURL=Outflows.js.map