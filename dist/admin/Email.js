"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var Email = (function (_super) {
    __extends(Email, _super);
    function Email() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Email.prototype.onEvent = function () {
        var local = this;
        $("body").on("click", "#email-add", function (event) {
            event.preventDefault();
            local.add(event, $(this));
        });
        $("body").on("click", "#btn-email-delete", function (event) {
            event.preventDefault();
            local.delete(event, $(this));
        });
    };
    Email.prototype.delete = function (event, element) {
        this.url = '/admin/default/email-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');
    };
    Email.prototype.onDelete = function (data) {
        $('#email-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    Email.prototype.add = function (event, element) {
        this.url = '/admin/default/email-add';
        this.data['email'] = $('[name="email"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    };
    Email.prototype.onAdd = function (data) {
        $('[name="email"]').val('');
        $('#email-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    return Email;
}(Send));
exports.Email = Email;
//# sourceMappingURL=Email.js.map