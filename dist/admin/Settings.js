"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var Settings = (function (_super) {
    __extends(Settings, _super);
    function Settings() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    //    public constructor()
    //    {}
    Settings.prototype.onEvent = function () {
        var local = this;
        $("body").on("click", "#btn-dollarExchangeRate-change", function (event) {
            event.preventDefault();
            local.dollarExchangeRate(event, $(this));
        });
        $("body").on("click", "#btn-cache-change", function (event) {
            event.preventDefault();
            local.cacheUpdate(event, $(this));
        });
    };
    Settings.prototype.cacheUpdate = function (event, element) {
        console.log('enter cache');
        this.url = '/admin/setting/cache-update';
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    Settings.prototype.dollarExchangeRate = function (event, element) {
        this.url = '/admin/setting/dollar-exchange-rate-change';
        this.data['dollarExchangeRate'] = $('#dollarExchangeRate').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onSuccess');
    };
    Settings.prototype.onSuccess = function (data) {
        this.showMessage(data.message, data.nameField);
    };
    return Settings;
}(Send));
exports.Settings = Settings;
//# sourceMappingURL=Settings.js.map