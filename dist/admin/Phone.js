"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var Phone = (function (_super) {
    __extends(Phone, _super);
    function Phone() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Phone.prototype.onEvent = function () {
        var local = this;
        $("body").on("click", "#phone-add", function (event) {
            event.preventDefault();
            local.add(event, $(this));
        });
        $("body").on("click", "#btn-phone-delete", function (event) {
            event.preventDefault();
            local.delete(event, $(this));
        });
    };
    Phone.prototype.delete = function (event, element) {
        this.url = '/admin/default/phone-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');
    };
    Phone.prototype.onDelete = function (data) {
        $('#phone-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    Phone.prototype.add = function (event, element) {
        this.url = '/admin/default/phone-add';
        this.data['phone'] = $('[name="phone"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    };
    Phone.prototype.onAdd = function (data) {
        $('[name="phone"]').val('');
        $('#phone-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    return Phone;
}(Send));
exports.Phone = Phone;
//# sourceMappingURL=Phone.js.map