"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var LoadFile = (function (_super) {
    __extends(LoadFile, _super);
    function LoadFile() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    //    public constructor(){}
    LoadFile.prototype.onEvent = function () {
        var local = this;
        $('[name="file-excel-load"]').change(function (event) {
            local.fileAjax = this.files;
            local.change(event, $(this));
        });
        $("body").on("click", "#btn-load-file", function (event) {
            event.preventDefault();
            local.fileAjax = $('[name="file-excel-load"]').prop("files")[0];
            console.log(local.fileAjax);
            local.change(event, $('[name="file-excel-load"]'));
        });
        $("body").on("click", '[btn-id="btn-excel-delete"]', function (event) {
            event.preventDefault();
            local.deleteExcelFile(event, $(this));
        });
    };
    LoadFile.prototype.deleteExcelFile = function (event, element) {
        this.url = '/admin/load-files/delete-excel';
        this.type = 'post';
        this.dataType = 'json';
        this.data['delete'] = $(element).attr('data-id');
        this.send('onDeleteExcelFile');
    };
    LoadFile.prototype.onDeleteExcelFile = function (data) {
        $("#table-excel tbody").html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    LoadFile.prototype.change = function (event, element) {
        this.url = '/admin/load-files/save-excel';
        this.type = 'post';
        this.dataType = 'json';
        var data = new FormData();
        $.each(this.fileAjax, function (key, value) {
            data.append(key, value);
        });
        this.data = data;
        this.processData = false; // Не обрабатываем файлы (Don't process the files)
        this.contentType = false; // Так jQuery скажет серверу что это строковой запрос this.processData = false;
        this.send('onLoad');
    };
    LoadFile.prototype.onLoad = function (data) {
        this.processData = true;
        this.contentType = "application/x-www-form-urlencoded";
        this.data = {};
        $("#table-excel tbody").html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    return LoadFile;
}(Send));
exports.LoadFile = LoadFile;
//# sourceMappingURL=LoadFile.js.map