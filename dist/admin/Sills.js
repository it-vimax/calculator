"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _Send = require("./Send");
var Send = _Send.Send;
var Sills = (function (_super) {
    __extends(Sills, _super);
    function Sills() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    //    public constructor(){}
    Sills.prototype.onEvent = function () {
        var local = this;
        $("body").on("click", "#btn-sills-add", function (event) {
            event.preventDefault();
            local.add(event, $(this));
        });
        $("body").on("click", "#btn-sill-delete", function (event) {
            event.preventDefault();
            local.delete(event, $(this));
        });
    };
    Sills.prototype.delete = function (event, element) {
        this.url = '/admin/window-items/sill-delete';
        this.data['delete'] = $(element).data('id');
        this.type = 'post';
        this.dataType = 'json';
        this.send('onDelete');
    };
    Sills.prototype.onDelete = function (data) {
        $('#sill-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    Sills.prototype.add = function (event, element) {
        this.url = '/admin/window-items/sill-add';
        this.data['sillDepth'] = $('[name="sill-new-depth"]').val();
        this.data['sillPrice'] = $('[name="sill-new-price"]').val();
        this.type = 'post';
        this.dataType = 'json';
        this.send('onAdd');
    };
    Sills.prototype.onAdd = function (data) {
        $('[name="sill-new-depth"]').val('');
        $('[name="sill-new-price"]').val('');
        $('#sill-table tbody').html(data.showTable);
        this.showMessage(data.message, data.nameField);
    };
    return Sills;
}(Send));
exports.Sills = Sills;
//# sourceMappingURL=Sills.js.map