"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Send = (function () {
    function Send() {
        this.data = {};
        this.type = "get";
        this.processData = true;
        this.dataType = 'text';
        this.contentType = "application/x-www-form-urlencoded";
    }
    Send.prototype.send = function (callback) {
        this.data['param'] = $('meta[name=csrf-param]').attr("content");
        this.data['token'] = $('meta[name=csrf-token]').attr("content");
        var local = this;
        $.ajax({
            url: local.url,
            //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            dataType: this.dataType,
            data: local.data,
            async: true,
            cache: true,
            contentType: this.contentType,
            processData: this.processData,
            type: local.type,
            success: function (data) {
                if (data.messageType !== 'success') {
                    local.error(data);
                    return;
                }
                var callbackMethod = 'local.' + callback + '(data)';
                eval(callbackMethod);
            },
            error: function (error) {
                console.log(error);
            },
            beforeSend: function () { },
            complete: function () { } //	срабатывает по окончанию запроса
        });
    };
    Send.prototype.error = function (data) {
        this.processData = true;
        this.contentType = "application/x-www-form-urlencoded";
        this.data = {};
        this.showMessage(data.message, data.nameField);
    };
    Send.prototype.showMessage = function (message, attribute, showTime) {
        if (showTime === void 0) { showTime = 2000; }
        $(attribute).html(message);
        $(attribute).fadeIn(10);
        setTimeout(function () {
            $(attribute).fadeOut(1000);
        }, showTime);
    };
    return Send;
}());
exports.Send = Send;
//# sourceMappingURL=Send.js.map