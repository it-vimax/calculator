"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Send = (function () {
    function Send() {
    }
    //    public constructor(){}
    Send.send = function (callback) {
        $.ajax({
            url: Basic.url,
            //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            //dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
            data: Basic.data,
            async: true,
            cache: true,
            contentType: "application/x-www-form-urlencoded",
            type: Basic.type,
            success: function (data) {
                if (!data) {
                    console.warn('false');
                    return;
                }
                eval('Email.' + callback + '(data)');
            },
            error: function (error) {
                console.log(error);
            },
            beforeSend: function () { },
            complete: function () { } //	срабатывает по окончанию запроса
        });
    };
    return Send;
}());
Send.data = {};
exports.Send = Send;
//# sourceMappingURL=Basic.js.map