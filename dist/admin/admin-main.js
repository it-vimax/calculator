"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Email = require("./Email");
var Email = _Email.Email;
var _Phone = require("./Phone");
var Phone = _Phone.Phone;
var _AdditionalWorks = require("./AdditionalWorks");
var AdditionalWorks = _AdditionalWorks.AdditionalWorks;
var _Sills = require("./Sills");
var Sills = _Sills.Sills;
var _Outflows = require("./Outflows");
var Outflows = _Outflows.Outflows;
var _LoadFile = require("./LoadFile");
var LoadFile = _LoadFile.LoadFile;
var _Promotions = require("./Promotions");
var Promotions = _Promotions.Promotions;
var _Settings = require("./Settings");
var Settings = _Settings.Settings;
var Main = (function () {
    function Main() {
    }
    Main.main = function () {
        // Обратная связь
        var email = new Email();
        email.onEvent();
        var phone = new Phone();
        phone.onEvent();
        // Компоненты окна
        var sills = new Sills();
        sills.onEvent();
        var outflows = new Outflows();
        outflows.onEvent();
        // дополнительные работы
        var additionalWorks = new AdditionalWorks();
        additionalWorks.onEvent();
        //  загрузка файла
        var loadFile = new LoadFile();
        loadFile.onEvent();
        // акции
        var promotions = new Promotions();
        promotions.onEvent();
        // настройки
        var settings = new Settings();
        settings.onEvent();
    };
    return Main;
}());
$(document).ready(function (event) {
    Main.main();
});
//# sourceMappingURL=admin-main.js.map