<?php

namespace app\libs;

use app\models\Outflows;
use app\models\Sills;
use PHPExcel_Cell;
use PHPExcel_IOFactory;

class Calculator
{
    public $typeWindow = 0;
    // получаем данные с excel
    public function getFromExcel($fileName, $tabName)
    {
        $excelArray = [];
        foreach($fileName as $key => $valueFileName)
        {
            try
            {
                $excels = PHPExcel_IOFactory::load($valueFileName); // подключиние к xls
            }
            catch(\Exception $e)
            {
                return ['warning' => [['файл не найден']]];
            }
            foreach ($excels->getWorksheetIterator() as $excel)
            {
                $tableTitle = $excel->getTitle();   //  имья таблицы
//                $excelArray = \Yii::$app->cache->get($valueFileName.$tabName[$key]);
//                if(!$excelArray)
//                {
                    if($excel->getTitle() === $tabName[$key])
                    {
                        $lastRow = $excel->getHighestRow(); //  последняя используемая строка
                        $lastColumn = $excel->getHighestColumn();   //  последний используемый столбец
                        $lastColumnIndex = PHPExcel_Cell::columnIndexFromString($lastColumn);   //  последний используемый индекс столбца
                        for ($row = 1; $row <= $lastRow; $row++)
                        {
                            for ($col = 0; $col < $lastColumnIndex; $col++)
                            {
                                $excelArray[$key][$col][$row] = $excel->getCellByColumnAndRow($col, $row)->getValue();
                            }
                        }
                    }
//                    \Yii::$app->cache->set($valueFileName.$tabName[$key], $excelArray, 60*60*24);
//                }
//                else
//                {
//                    \Yii::$app->cache->set($valueFileName.$tabName[$key], $excelArray, 60*60*24);
//                }
            }
            if(empty($excelArray))
            {
                $excelArray = ['warning' => 'Таблица не найдена'];
            }
        }
        return $excelArray;
    }

    // выводим максимальные и минимальные размеры окна
    public function getSize($windowType, $winArr)
    {
        $windowSize = [];
        foreach($windowType as $keyWindowType=>$value)
        {
            $excelArray = [];
            $excel = PHPExcel_IOFactory::load($value);
            foreach($excel->getWorksheetIterator() as $key => $excel)
            {
                if($key == 0)
                {
                    $lastRow = $excel->getHighestRow(); //  последняя используемая строка
                    $lastColumn = $excel->getHighestColumn();   //  последний используемый столбец
                    $lastColumnIndex = PHPExcel_Cell::columnIndexFromString($lastColumn);   //  последний используемый индекс столбца
                    for ($row = 1; $row <= $lastRow; $row++)
                    {
                        for ($col = 0; $col < $lastColumnIndex; $col++)
                        {
                            $excelArray[$key][$col][$row] = $excel->getCellByColumnAndRow($col, $row)->getValue();
                        }
                    }
                }
                break;
            }
            $resSize = [];
            foreach($excelArray as $keyExcelArray => $valueExcelArray)
            {
                $resSize['height'] = $valueExcelArray[0][2];
                $resSize['width'] = $valueExcelArray[1][1];
                for($i=2; $i<count($valueExcelArray[0]); $i++)
                {
                    if($valueExcelArray[0][$i] == null)
                    {
                        $resSize['maxHeight'] = $valueExcelArray[0][$i-1];
                        break;
                    }
                }
                for($i2=1; $i2<count($valueExcelArray)+1; $i2++)
                {
                    if($valueExcelArray[$i2][1] == null)
                    {
                        $resSize['maxWidth'] = $valueExcelArray[$i2-1][1];
                        break;
                    }
                }
            }
            $windowSize['w'.$winArr[$keyWindowType]] = $resSize;
        }
        return $windowSize;
    }

    // формируем цену за окно
    public function getWindowPrice($width, $height, $widthBalkon, $heightBalkon, $excel)
    {
        $priceWindow = 0;
        foreach($excel as $keyExcel=>$valueExcel)
        {
            if(count($excel) > 1 && $keyExcel == 0)
            {
                $fileWidth = $valueExcel[1][1];
                $fileHeight = $valueExcel[0][2];
                $numValHeight = (($heightBalkon - $fileHeight) / 50) <= 0 ? 2 : (($heightBalkon - $fileHeight) / 50) + 2; // показываем текущий рядок
                $numValWidth = (($widthBalkon - $fileWidth) / 50) <= 0 ? 1 : (($widthBalkon - $fileWidth) / 50) + 1; // показываем текущую колонку
                $priceWindow += $valueExcel[$numValWidth][$numValHeight]; // цена окна в долларах
            }
            else
            {
                $fileWidth = $valueExcel[1][1];
                $fileHeight = $valueExcel[0][2];
                $numValHeight = (($height - $fileHeight) / 50) <= 0 ? 2 : (($height - $fileHeight) / 50) + 2; // показываем текущий рядок
                $numValWidth = (($width - $fileWidth) / 50) <= 0 ? 1 : (($width - $fileWidth) / 50) + 1; // показываем текущую колонку
                $priceWindow += $valueExcel[$numValWidth][$numValHeight]; // цена окна в долларах
            }
        }
        return $priceWindow;
    }

    // получаем подоконники
    public function getSill($widthSill, $widthWindow, $heightWindow, $widthDor = null, $heightDor = null)
    {
        $sill = Sills::find()->where(['depth' => $widthSill])->limit(1)->all()[0]; // данные с базы
        $res = round((($widthWindow + 100) / 1000) * $sill->price);
        return $res;
    }
    // получаем отливы
    public function getOutflows($widthOutflows, $widthWindow, $heightWindow, $heightDor = null, $widthDor = null)
    {
        $outfows = Outflows::find()->where(['depth' => $widthOutflows])->limit(1)->all()[0];
        $res = round((($widthWindow + 100) / 1000) * $outfows->price);
        return $res;
    }
    // считаем откосы
    public function getSlopes($height, $width, $widthBalkon, $heightBalkon, $slopes)
    {
        $h2 = $height / 1000;
        $w2 = $width / 1000;
        $hBalkon2 = $heightBalkon / 1000;
        $wBalkon2 = $widthBalkon / 1000;
        $total = 0;
        if($this->typeWindow == 0)
        {
            $total = ($w2 + $h2 + $h2) * $slopes;
        }
        elseif($this->typeWindow == 1)
        {
            $total = ($hBalkon2 + $w2 + $wBalkon2 + $h2) * $slopes;
        }
        return $total;
    }
    // считаем монтаж
    public function getWindowInstalation($height, $width, $widthBalkon, $heightBalkon, $windowInstalation)
    {
        $h2 = $height / 1000;
        $w2 = $width / 1000;
        $hBalkon2 = $heightBalkon / 1000;
        $wBalkon2 = $widthBalkon / 1000;
        $total = 0;
        if($this->typeWindow == 0)
        {
            $total = ($w2 * $h2) * $windowInstalation;
        }
        elseif($this->typeWindow == 1)
        {
            $w = ($w2 * $h2) * $windowInstalation;
            $b = ($wBalkon2 * $hBalkon2) * $windowInstalation;
            $total = $w + $b;
        }
        return $total;
    }
    
    public function convertNameItem($typeWindow, $profile, $steklopaket, $furnitura)
    {
        $res = [];
        switch($typeWindow)
        {
            case '1g':
                $res['typeWindow'] = 'Одностворчатое глухое';
                break;
            case '1p':
                $res['typeWindow'] = 'Одностворчатое поворотно-откидное';
                break;
            case '2gp':
                $res['typeWindow'] = 'Двухстворчатое: глухое, поворотно-откидное';
                break;
            case '3gpf':
                $res['typeWindow'] = 'Двухстворчатое с фрамугой: глухое, поворотно-откидное';
                break;
            case '3gpg':
                $res['typeWindow'] = 'Трехстворчатое: глухое, поворотно-откидное, глухое';
                break;
            case '3pgp':
                $res['typeWindow'] = 'Трехстворчатое: поворотно-откидное, глухое, поворотно-откидное';
                break;
            case '4gppg':
                $res['typeWindow'] = 'Четырехстворчатое: 2 - глухие, 2 – поворотно-откидные';
                break;
            case '1b1g':
                $res['typeWindow'] = ' Балконный блок: одностворчатое глухое окно';
                break;
            case '1b2gp':
                $res['typeWindow'] = ' Балконные блок: двухстворчатое: глухое, поворотно-откидное';
                break;

        }
        switch($profile)
        {
            case 'lid':
                $res['profile'] = 'Lider';
                break;
            case 'b58':
                $res['profile'] = 'Viknaland B58';
                break;
            case 'b70':
                $res['profile'] = 'Viknaland B70';
                break;
            case 'Al4':
                $res['profile'] = 'Aluplast Ideal 4000';
                break;

        }
        switch($steklopaket)
        {
            case '24-':
                $res['steklopaket'] = 'Однокамерный';
                break;
            case '32i':
                $res['steklopaket'] = 'Двухкамер. + энерго';
                break;
            case '32M':
                $res['steklopaket'] = 'Двухкамер. + мультифунк.';
                break;

        }
        switch ($furnitura)
        {
            case 'ax':
                $res['furnitura'] = 'Axor (Украина)';
                break;
            case 'si':
                $res['furnitura'] = 'Siegenia (Германия)';
                break;
        }
        return $res;
    }
}