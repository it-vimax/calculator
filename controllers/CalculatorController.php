<?php
namespace app\controllers;

use app\libs\Calculator;
use app\models\AdditionalWorks;
use app\models\Comunications;
use app\models\Emails;
use app\models\Files;
use app\models\Percent;
use app\models\Phones;
use app\models\Promotions;
use app\models\Settings;
use yii\db\Exception;
use yii\helpers\Url;
use yii\web\Response;
use Yii;
use yii\web\Controller;

class CalculatorController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|Response
     */
    public function actionCalculationAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(/*Yii::$app->request->isAjax &&*/ Yii::$app->request->isGet)
        {
            $calculator = new Calculator();
            switch(Yii::$app->request->get('data')['lamination'])
            {
                case 0:
                    $data['color'] = 'w';
                    break;
                case 1:
                    $data['color'] = 'c';
                    break;
            }
            $data['contentSliderHeight'] = Yii::$app->request->get('data')['contentSliderHeight'];
            $data['contentSliderHeightRight'] = Yii::$app->request->get('data')['contentSliderHeightRight'];
            $data['contentSliderWidth'] = Yii::$app->request->get('data')['contentSliderWidth'];
            $data['contentSliderWidthTop'] = Yii::$app->request->get('data')['contentSliderWidthTop'];
            $data['montaj'] = Yii::$app->request->get('data')['montaj'];
            $data['moskit'] = Yii::$app->request->get('data')['moskit'];
            $data['musor'] = Yii::$app->request->get('data')['musor'];
            $data['otkos'] = Yii::$app->request->get('data')['otkos'];
            $data['typeWindow'] = Yii::$app->request->get('data')['typeWindow'];
            $data['furnituraVal'] = Yii::$app->request->get('data')['characteristicsWindow']['furnituraVal'];
            $data['otlivVal'] = Yii::$app->request->get('data')['characteristicsWindow']['otlivVal'];
            $data['podokonnikVal'] = Yii::$app->request->get('data')['characteristicsWindow']['podokonnikVal'];
            $data['profileVal'] = Yii::$app->request->get('data')['characteristicsWindow']['profileVal'];
            $data['steklopaketVal'] = Yii::$app->request->get('data')['characteristicsWindow']['steklopaketVal'];
            if(empty($data['profileVal']))
            {
                return $this->asJson(['warning' => 'Выберите профиль! Для просчета данных калькулятору нужны минимальные данные, такие, как фурнитура, стеклопакет, профиль.']);
            }
            if(empty($data['steklopaketVal']))
            {
                return $this->asJson(['warning' => 'Выберите стеклопакет! Для просчета данных калькулятору нужны минимальные данные, такие, как фурнитура, стеклопакет, профиль.']);
            }
            if($data['typeWindow'] == "1g")
            {
                $data['furnituraVal'] = 'ax';
            }
            if(empty($data['furnituraVal']))
            {
                return $this->asJson(['warning' => 'Выберите фурнитуру! Для просчета данных калькулятору нужны минимальные данные, такие, как фурнитура, стеклопакет, профиль.']);
            }
            // формируем название таблицы
            if($data['typeWindow'] == '1b1g')
            {
                $calculator->typeWindow = 1;
                $fileName[0] = '1b,'.$data['color'];
                $fileName[1] = '1g,'.$data['color'];
                $tabName[0] = '1b,'
                    .$data['profileVal'].','
                    .$data['furnituraVal'].','
                    .$data['color'].','
                    .$data['steklopaketVal'];
                $tabName[1] = '1g,'
                    .$data['profileVal'].','
                    .$data['furnituraVal'].','
                    .$data['color'].','
                    .$data['steklopaketVal'];
            }
            elseif($data['typeWindow'] == '1b2gp')
            {
                $calculator->typeWindow = 1;
                $fileName[0] = '1b,'.$data['color'];
                $fileName[1] = '2gp,'.$data['color'];
                $tabName[0] = '1b,'
                    .$data['profileVal'].','
                    .$data['furnituraVal'].','
                    .$data['color'].','
                    .$data['steklopaketVal'];
                $tabName[1] = '2gp,'
                    .$data['profileVal'].','
                    .$data['furnituraVal'].','
                    .$data['color'].','
                    .$data['steklopaketVal'];
            }
            else
            {
                $calculator->typeWindow = 0;
                $fileName[0] = $data['typeWindow'].','.$data['color'];
                $tabName[0] = $data['typeWindow'].','
                    .$data['profileVal'].','
                    .$data['furnituraVal'].','
                    .$data['color'].','
                    .$data['steklopaketVal'];
            }
            $file = [];
            foreach($fileName as $key => $item)
            {
                $file[$key] = Files::find()->where(['name' => $item])->limit(1)->all()[0];
            }

            if(count($file) > 0)
            {
                $fileUrl = [];
                foreach($file as $key => $item)
                {
                    $fileUrl[$key] = Url::to($item->path) . $item->name . $item->extension; // путь к файлу
                }

                $excelArray = $calculator->getFromExcel($fileUrl, $tabName); // получаем массив с excel
                if($excelArray['warning'])
                {
                    return $this->asJson(['warning' => [[$excelArray['warning']]]]); // если не найден файл
                }
                $priceWindowUSA = $calculator->getWindowPrice( // получаю цену за окно
                    $data['contentSliderWidth'],
                    $data['contentSliderHeight'],
                    $data['contentSliderWidthTop'],
                    $data['contentSliderHeightRight'],
                    $excelArray
                );
                $sillTotalPriceUA = 0;
                if(isset($data['podokonnikVal']) && !empty($data['podokonnikVal'])) // +
                {
                    $sillTotalPriceUA = $calculator->getSill($data['podokonnikVal'], $data['contentSliderWidth'], $data['contentSliderHeight'], $data['contentSliderHeightRight'], $data['contentSliderWidthTop']); // формироуем цену подоконника
                }
                $outflowsTotalPriceUA = 0;
                if(isset($data['otlivVal']) && !empty($data['otlivVal'])) // +
                {
                    $outflowsTotalPriceUA = $calculator->getOutflows($data['otlivVal'], $data['contentSliderWidth'], $data['contentSliderHeight'], $data['contentSliderHeightRight'], $data['contentSliderWidthTop']); // формируем цену отлива
                }
                $additionalWorksArr = []; // получаем список настроек
                $additionalWorks = AdditionalWorks::find()->all();
                foreach($additionalWorks as $additionalWork)
                {
                    $additionalWorksArr[$additionalWork->name] = $additionalWork->value;
                }
                unset($additionalWorks);
                $slopesTotalPriceUA = 0;
                if($data['otkos'])
                {
                    $slopesTotalPriceUA = $calculator->getSlopes(
                        $data['contentSliderHeight'],
                        $data['contentSliderWidth'],
                        $data['contentSliderWidthTop'],
                        $data['contentSliderHeightRight'],
                        $additionalWorksArr['slopes']); // считаем otkosi
                }
                $windowInstalationTotalPrice = 0;
                if($data['montaj']) //+
                {
                    $windowInstalationTotalPrice = $calculator->getWindowInstalation(
                        $data['contentSliderHeight'],
                        $data['contentSliderWidth'],
                        $data['contentSliderWidthTop'],
                        $data['contentSliderHeightRight'],
                        $additionalWorksArr['windowInstalation']); // считаем montazh
                }
                $mosquitoNet = 0;
                if($data['moskit'])
                {
                    $mosquitoNet =$additionalWorksArr['mosquitoNet']; // считаем москитную сетку
                }
                $gorbageRemoval = 0;
                if($data['musor'])
                {
                    $gorbageRemoval = $additionalWorksArr['gorbageRemoval']; // считаем мусоо
                }
                // получаем массив настроек
                $settingArr = [];
                $setting = Settings::find()->all();
                foreach($setting as $item)
                {
                    $settingArr[$item->name] = $item->value;
                }
                unset($setting);
                // скидки и акции
                $promotionArr = [];
                $promotins = Promotions::find()->all();
                foreach($promotins as $promotin)
                {
                    $promotionArr[$promotin->name] = $promotin->value;
                }
                unset($promotins);
                // подсчитываем всю сумму
                if(!isset($settingArr["dollarExchangeRate"]) || $settingArr["dollarExchangeRate"] <= 0)
                {
                    $settingArr["dollarExchangeRate"] = 1;
                }
                $priceWindowUAOrigin = $priceWindowUSA * $settingArr["dollarExchangeRate"];
                $priceWindowUA = round($priceWindowUAOrigin - (($priceWindowUAOrigin / 100) * (integer)$promotionArr['discountOnWindows']), 0); // стоимость окна с учетом процента скидки на окна
                $totalPrice = round($priceWindowUA
                    + $outflowsTotalPriceUA
                    + $sillTotalPriceUA
                    + $slopesTotalPriceUA
                    + $windowInstalationTotalPrice
                    + $mosquitoNet
                    + $gorbageRemoval, 0);
                $proc = (100 - $promotionArr['generalAdvertisingDiscount']) /100;
                $totalPrice = round($totalPrice / $proc);
                $salePrice = round($totalPrice * $proc); // сума на вывод клиенту без скидки
                session_start();
                $_SESSION['totalPrice']['window']['width'] = $data['contentSliderWidth'];
                $_SESSION['totalPrice']['window']['hight'] = $data['contentSliderHeight'];
                $_SESSION['totalPrice']['window']['price'] = $priceWindowUA;
                $convertNameItemWindowArr = $calculator->convertNameItem($data['typeWindow'], $data['profileVal'], $data['steklopaketVal'], $data['furnituraVal']);
                $_SESSION['totalPrice']['window']['type'] = $convertNameItemWindowArr['typeWindow'];
                $_SESSION['totalPrice']['window']['profile'] =  $convertNameItemWindowArr['profile'];
                $_SESSION['totalPrice']['window']['steklopaket'] =  $convertNameItemWindowArr['steklopaket'];
                if(!empty($data['furnituraVal']))
                {
                    $_SESSION['totalPrice']['window']['furnitura'] = $convertNameItemWindowArr['furnitura'];
                }
                if($data['typeWindow'] == '1b1g' || $data['typeWindow'] == '1b2gp')
                {
                    $_SESSION['totalPrice']['dor']['width'] = $data['contentSliderWidthTop'];
                    $_SESSION['totalPrice']['dor']['hight'] = $data['contentSliderHeightRight'];
                }
                $_SESSION['totalPrice']['color'] = $data['color'];
                if(!empty($data['otlivVal']))
                {
                    $_SESSION['totalPrice']['otliv']['size'] = $data['otlivVal'];
                    $_SESSION['totalPrice']['otliv']['price'] = $outflowsTotalPriceUA;
                }
                if(!empty($data['podokonnikVal']))
                {
                    $_SESSION['totalPrice']['podokonnik']['size'] = $data['podokonnikVal'];
                    $_SESSION['totalPrice']['podokonnik']['price'] = $sillTotalPriceUA;
                }
                if(!empty($data['otkos']))
                {
                    $_SESSION['totalPrice']['otkosi']['size'] = $data['otkos'];
                    $_SESSION['totalPrice']['otkosi']['price'] = $slopesTotalPriceUA;
                }
                if(!empty($windowInstalationTotalPrice))
                {
                    $_SESSION['totalPrice']['montazh'] = $windowInstalationTotalPrice;
                }
                if(!empty($mosquitoNet))
                {
                    $_SESSION['totalPrice']['moskitnai_setka'] = $mosquitoNet;
                }
                if(!empty($gorbageRemoval))
                {
                    $_SESSION['totalPrice']['vivoz_musora'] = $gorbageRemoval;
                }
                $_SESSION['totalPrice']['total_price'] = $totalPrice;
                $_SESSION['totalPrice']['sale']['percent'] = $promotionArr["generalAdvertisingDiscount"];
                $_SESSION['totalPrice']['sale']['total'] = $salePrice;
                $phones = Phones::find()->all();
                return $this->renderAjax('calcResult', compact('promotionArr', 'phones', 'salePrice'));
            }
            else
            {
                return $this->asJson(['error' => [['файл не найден']]]);
            }
        }
        return $this->asJson(['error' => [['No ajax']]]);
    }

    public function actionGetSizeWindow()
    {
        $windowType =Yii::$app->request->get('windowType');
        if(!empty($windowType))
        {
            $winArr = [];
            switch ($windowType)
            {
                case '1b1g':
                    $winArr = ['1b', '1g'];
                    break;
                case '1b2gp':
                    $winArr = ['1b', '2gp'];
                    break;
                default:
                    $winArr = [$windowType];

            }
            $pathFile = [];
            foreach($winArr as $keyWinArr=>$valueWinArr)
            {
                $pathFile[$keyWinArr] = Files::find()->where(['name' => $valueWinArr.',c'])->limit(1)->all()[0];
            }
            $fileUrl = [];
            if(count($pathFile) > 0) {
                $fileUrl = [];
                foreach ($pathFile as $key => $item)
                {
                    $fileUrl[$key] = Url::to($item->path) . $item->name . $item->extension; // путь к файлу
                }
            }
            $calculator = new Calculator();
            $windowSize = $calculator->getSize($fileUrl, $winArr);
            return $this->asJson($windowSize);
        }
        else
        {
            return $this->redirect('/');
        }
    }

    public function actionCallMe()
    {
        if(Yii::$app->request->isPost)
        {
            $client['name'] = Yii::$app->request->post('name');
            $client['email'] = Yii::$app->request->post('email');
            $client['phone'] = Yii::$app->request->post('phone');
            $client['yourQuestion'] = Yii::$app->request->post('ask');
            if(!empty(Yii::$app->request->post('other_consultation')))
            {
                $client['other_consultation'] = Yii::$app->request->post('other_consultation');
            }
            $res = [];
            //  Отправка письма
            if(!empty($client['name']) && !empty($client['phone']))
            {
                $emails = Emails::find()->asArray()->all();
                if(count($emails) > 0)
                {
                    foreach($emails as $email)
                    {
                        $mailRes = Yii::$app->mailer->compose('calculator', ['client' => $client])
                            ->setFrom(Yii::$app->params['calculatorEmail'])
                            ->setTo($email['value'])
                            ->setSubject('Заказ окон с калькулятора')
                            ->send();
                    }
                }
                unset($_SESSION['totalPrice']);
                $res['success'] = 'Заказ принят';
            }
            else
            {
                $res['error'] = 'Вы не заполнили все поля для отправки заказа!';
            }
            return $this->asJson($res);
        }
        else
        {
            return $this->redirect('/');
        }
    }
}