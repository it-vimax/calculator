rbac migration:
yii migrate --migrationPath=@mdm/admin/migrations
yii migrate --migrationPath=@yii/rbac/migrations
yii migrate

webpack:
webpack dist/index.js main.js --watch --optimize-minimize
webpack dist/admin/admin-main.js admin-main.js --watch --optimize-minimize

------------------- Front-end ----------------
Добавить фурнитуру в первое окно
При клике в сторону мышь нужно убирать список

------------------- Back-end ----------------
Проверить формулу просчета параметров = Подоконники и отливы, Откосы
Добавлять ли квадратуру просчета к дверям, или только к окнам?
Окно с балконом без фурнитруты не наботаер