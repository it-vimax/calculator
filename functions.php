<?php

function dump($arr)
{
    echo '<pre>' . print_r($arr, true) . '</pre>';
}

function dd($arr)
{
    dump($arr);
    exit();
}

function vdump($arr)
{
    echo '<pre>' . var_dump($arr, true) . '</pre>';
}

function vd($arr)
{
    vdump($arr);
    exit();
}